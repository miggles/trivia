// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import css from "../css/app.css"

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import dependencies
//
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative paths, for example:
// import socket from "./socket"

// Start LiveView.
import LiveSocket from "phoenix_live_view"

let liveSocket = new LiveSocket("/live")
liveSocket.connect()

// Load our one sound effect
var sound = new Howl({
  src: ['/sounds/Tick-DeepFrozenApps-397275646.mp3']
});

// Websocket connection for streaming live updates without annoying live view
//
// This is all very very bad and stems completely from being unable to put a
// a form and a list of other people's bids in the same row without having
// liveview replace the form on every other bid.
//
// There has to be a better way. Drab could do this better.
import socket from "./socket"
socket.connect()

var Game = {
  bids: {},

  subscribeToGame: function (game_id) {
    this.channel = socket.channel("game:" + game_id, {})
    this.channel.join()
      .receive("ok", resp => { true })
      .receive("error", resp => { false })

    // Start a timer to populate/replace lists of bids periodically
    setInterval(function() {
      // Scan for game question answer bid lists
      document.querySelectorAll("ul.game_question_answer_bid_list").forEach(function(ul) {
        let gameQuestionAnswerId = parseInt(ul.dataset.gameQuestionAnswerId)

        if (Game.bids[gameQuestionAnswerId]) {
          let liCount = ul.childNodes.length - 1

          // Add any missing bids
          Game.bids[gameQuestionAnswerId].slice(liCount - 1).forEach(function(li) {
            ul.appendChild(li)
          })
        }

      })
    }, 50)

    this.channel.on("bid_created", bid => {
      this.bids[bid.game_question_answer_id] = this.bids[bid.game_question_answer_id] || []

      var ul = document.createElement("ul")
      ul.innerHTML = bid.html
      this.bids[bid.game_question_answer_id].push(ul.firstChild)
    })

    this.channel.on("tick", tick => {
      var volume = 1 - ((tick.time_remaining - 1) * 0.1);

      if (volume > 0) {
        // sound.stop();
        sound.volume(volume);
        sound.play();
      }
    })
  }
}

// And finally start our game process.
var elem = document.getElementById("start_game_socket")
if (elem) {
  Game.subscribeToGame(elem.value)
}

// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
function debounce(func, wait, immediate) {
  var timeout;
  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};

// TODO: All of this needs to be recalculated after a window resize.
var elem = document.querySelector(".game-state")
if (elem) {
  var offsetHeight = elem.parentNode.offsetHeight;
  var headerHeight = document.querySelector(".header").parentNode.offsetHeight;

  var affix = debounce(function(e) {
    var doc = document.documentElement;
    var left = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
    var top = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);

    if (top >= headerHeight) {
      elem.parentNode.style = "height: " + offsetHeight + "px";
      elem.classList.add("fixed")
    }
    else {
      elem.classList.remove("fixed")
      elem.parentNode.style = "";
    }
  }, 25)

  window.addEventListener("scroll", affix)
  affix()
}
