defmodule Trivia.Repo.Migrations.CreateGameQuestionUsers do
  use Ecto.Migration

  def change do
    create table(:game_question_users) do
      add :answer, :integer
      add :game_question_id, references(:game_questions, on_delete: :nothing)
      add :game_user_id, references(:game_users, on_delete: :nothing)

      timestamps()
    end

    create index(:game_question_users, [:game_question_id])
    create index(:game_question_users, [:game_user_id])
  end
end
