defmodule Trivia.Repo.Migrations.AddBonusPayoutFieldsToGameQuestionUsers do
  use Ecto.Migration

  def change do
    alter table(:game_question_answer_game_users) do
      add :winning_answer_payout, :integer, default: 0, null: false
      add :correct_answer_payout, :integer, default: 0, null: false
    end
  end
end
