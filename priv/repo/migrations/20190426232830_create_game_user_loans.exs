defmodule Trivia.Repo.Migrations.CreateGameUserLoans do
  use Ecto.Migration

  def change do
    create table(:game_user_loans) do
      add :amount, :integer, null: false
      add :amount_owing, :integer, null: false
      add :interest_rate, :float, null: false
      add :interest_rate_string, :string, null: false
      add :game_user_id, references(:game_users, on_delete: :nothing)

      timestamps()
    end

    create index(:game_user_loans, [:game_user_id])
  end
end
