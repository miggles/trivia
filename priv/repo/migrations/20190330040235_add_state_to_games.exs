defmodule Trivia.Repo.Migrations.AddStateToGames do
  use Ecto.Migration

  def change do
    alter table(:games) do
      add :state, :string, null: false
      add :state_info, :string
      add :state_started_at, :utc_datetime
      add :state_ends_at, :utc_datetime
    end
  end
end
