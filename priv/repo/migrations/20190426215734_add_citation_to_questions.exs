defmodule Trivia.Repo.Migrations.AddCitationToQuestions do
  use Ecto.Migration

  def change do
    alter table(:questions) do
      add :citation, :text
      add :citation_html, :text
    end
  end
end
