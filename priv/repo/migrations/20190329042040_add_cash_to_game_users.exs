defmodule Trivia.Repo.Migrations.AddCashToGameUsers do
  use Ecto.Migration

  def change do
    alter table(:game_users) do
      add :cash, :integer, default: 100, null: false
    end
  end
end
