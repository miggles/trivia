defmodule Trivia.Repo.Migrations.AddCurrentGameQuestionIdToGames do
  use Ecto.Migration

  def change do
    alter table(:games) do
      add :current_game_question_id, references(:game_questions, on_delete: :nothing)
    end
  end
end
