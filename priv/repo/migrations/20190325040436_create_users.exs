defmodule Trivia.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add(:username, :string)
      add(:uuid, :string)

      timestamps()
    end

    create(unique_index(:users, :uuid))
  end
end
