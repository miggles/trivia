defmodule Trivia.Repo.Migrations.AddSlugToGames do
  use Ecto.Migration

  def change do
    alter table(:games) do
      add :slug, :string, null: false
    end

    create unique_index(:games, [:slug])
  end
end
