defmodule Trivia.Repo.Migrations.AddLoansOwingToGameUser do
  use Ecto.Migration

  def change do
    alter table(:game_users) do
      add :loans_owing, :integer, default: 0, null: false
    end
  end
end
