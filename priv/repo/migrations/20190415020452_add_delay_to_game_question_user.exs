defmodule Trivia.Repo.Migrations.AddDelayToGameQuestionUser do
  use Ecto.Migration

  def change do
    alter table(:game_question_users) do
      add :delay, :integer
    end
  end
end
