defmodule Trivia.Repo.Migrations.AddOtherHtmlToQuestions do
  use Ecto.Migration

  def change do
    alter table(:questions) do
      add :question_html, :text
    end
  end
end
