defmodule Trivia.Repo.Migrations.AddQuestionsToGame do
  use Ecto.Migration

  def change do
    alter table(:games) do
      add :questions, :integer, null: false
    end
  end
end
