defmodule Trivia.Repo.Migrations.AddStartingCashToGames do
  use Ecto.Migration

  def change do
    alter table(:games) do
      add :starting_cash, :integer, default: 100, null: false
    end
  end
end
