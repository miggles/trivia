defmodule Trivia.Repo.Migrations.CreateQuestions do
  use Ecto.Migration

  def change do
    create table(:questions) do
      add :question, :string
      add :unit, :string
      add :answer, :integer
      add :additional, :string
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:questions, [:user_id])
  end
end
