defmodule Trivia.Repo.Migrations.AddPaidToGameQuestionAnswerGameUserBids do
  use Ecto.Migration

  def change do
    alter table(:game_question_answer_game_user_bids) do
      add :paid, :boolean, default: false, null: false
    end
  end
end
