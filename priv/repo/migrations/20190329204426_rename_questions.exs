defmodule Trivia.Repo.Migrations.RenameQuestions do
  use Ecto.Migration

  def change do
    rename table(:games), :questions, to: :number_of_questions
  end
end
