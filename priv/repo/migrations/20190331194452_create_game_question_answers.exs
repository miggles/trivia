defmodule Trivia.Repo.Migrations.CreateGameQuestionAnswers do
  use Ecto.Migration

  def change do
    create table(:game_question_answers) do
      add :answer, :integer
      add :winner, :boolean, default: false, null: false
      add :correct, :boolean, default: false, null: false
      add :payout_string, :string
      add :payout_rate, :float
      add :game_question_id, references(:game_questions, on_delete: :nothing)

      timestamps()
    end

    create index(:game_question_answers, [:game_question_id])
  end
end
