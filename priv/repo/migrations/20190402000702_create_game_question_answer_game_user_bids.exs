defmodule Trivia.Repo.Migrations.CreateGameQuestionAnswerGameUserBids do
  use Ecto.Migration

  def change do
    create table(:game_question_answer_game_user_bids) do
      add :bid, :integer, null: false
      add :payout, :integer
      add :game_question_answer_id, references(:game_question_answers, on_delete: :nothing)
      add :game_user_id, references(:game_users, on_delete: :nothing)

      timestamps()
    end

    create index(:game_question_answer_game_user_bids, [:game_question_answer_id])
    create index(:game_question_answer_game_user_bids, [:game_user_id])
  end
end
