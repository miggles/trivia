defmodule Trivia.Repo.Migrations.AddPeriodsToGames do
  use Ecto.Migration

  def change do
    alter table(:games) do
      remove(:state_info, :string)
      add(:current_question, :integer)

      add(:wait_duration, :integer, null: false)
      add(:ask_duration, :integer, null: false)
      add(:bid_duration, :integer, null: false)
      add(:display_duration, :integer, null: false)
    end
  end
end
