defmodule Trivia.Repo.Migrations.CreateGameQuestionAnswerGameUsers do
  use Ecto.Migration

  def change do
    create table(:game_question_answer_game_users) do
      add :game_user_id, references(:game_users, on_delete: :nothing)
      add :game_question_answer_id, references(:game_question_answers, on_delete: :nothing)

      timestamps()
    end

    create index(:game_question_answer_game_users, [:game_user_id])
    create index(:game_question_answer_game_users, [:game_question_answer_id])
  end
end
