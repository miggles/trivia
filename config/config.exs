# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :trivia,
  ecto_repos: [Trivia.Repo]

# Configures the endpoint
config :trivia, TriviaWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "brb9Lzx2QztHenDLgaJt7qPZcywJ5cLmVqZSWKfXNkv9/z9ZWjG46KMQ6WAGMwP0",
  render_errors: [view: TriviaWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Trivia.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [
    signing_salt: "O9RHbi0mQLtFWWYmg/M5+xMhX3yvMezNPOkAnom3gybOfwVye9N/xKL1ruWfy0Zy"
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :phoenix,
  template_engines: [leex: Phoenix.LiveView.Engine]

config :trivia,
  minimum_value: 0,
  maximum_value: 999_999_999,
  maximum_answers: 25

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
