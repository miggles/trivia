defmodule ProcessRegistry.Supervisor do
  defmacro __using__(opts) do
    quote location: :keep, bind_quoted: [opts: opts] do
      @worker opts[:worker]

      use DynamicSupervisor

      def start_link(_) do
        DynamicSupervisor.start_link(__MODULE__, :ok, name: __MODULE__)
      end

      @doc """
      Registers a new worker, and creates the worker process.
      """
      def register(id) do
        DynamicSupervisor.start_child(__MODULE__, {@worker, id})
      end

      def init(_) do
        DynamicSupervisor.init(strategy: :one_for_one)
      end
    end
  end
end
