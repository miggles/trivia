defmodule ProcessRegistry.Worker do
  defmacro __using__(opts) do
    quote location: :keep, bind_quoted: [opts: opts] do
      @supervisor opts[:supervisor]

      use GenServer, restart: opts[:restart] || :temporary

      require Logger

      def start_link(id) do
        GenServer.start_link(__MODULE__, id,
          name: {:via, Registry, {ProcessRegistry, name_for(id)}}
        )
      end

      def find_or_create_process(id, opts \\ []) do
        case Registry.lookup(ProcessRegistry, name_for(id)) do
          [{pid, _}] ->
            pid

          [] ->
            case @supervisor.register(id) do
              {:error, {:already_started, pid}} -> pid
              {:ok, pid} -> pid
            end
        end
      end

      def name_for(id), do: {__MODULE__, id}

      def with_ref(id, handler, retries \\ 2) do
        ref = find_or_create_process(id)
        try_with_ref(ref, id, handler, retries)
      end

      defp try_with_ref(ref, id, handler, retries) do
        handler.(ref)
      catch
        :exit, reason ->
          case handle_exit(ref, id, retries, reason) do
            {:fail, _} ->
              exit(reason)

            {:retry, retries} ->
              with_ref(id, handler, retries)
          end
      end

      defp handle_exit(ref, id, retries, reason) do
        {retry_message, retries, event} =
          case retries do
            false ->
              {false, false, :fail}

            _ ->
              retries = retries - 1

              cond do
                retries < 0 ->
                  {"retries exceeded, exiting", retries, :fail}

                retries == 0 ->
                  {"retries exhausted, exiting", retries, :fail}

                true ->
                  {false, retries, :retry}
              end
          end

        if retry_message do
          Logger.warn(
            "#{__MODULE__} #{id |> inspect} #{ref |> inspect} #{reason |> inspect}, #{
              retry_message
            }"
          )
        end

        {event, retries}
      end
    end
  end
end
