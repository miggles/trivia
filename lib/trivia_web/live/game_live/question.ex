defmodule TriviaWeb.GameLive.Question do
  use Phoenix.LiveView

  alias TriviaWeb.GameView

  alias Trivia.{
    Games,
    Games.GameQuestionUser,
    Games.GameUser,
    GameQuestionService,
    GameService,
    Loanshark,
    Repo
  }

  def mount(%{game_id: game_id, user_id: user_id}, socket) do
    game_user = Repo.get_by(GameUser, game_id: game_id, user_id: user_id)

    socket =
      socket
      |> assign(:game_id, game_id)
      |> assign(:game_user_id, game_user.id)
      |> assign(:game_user, game_user)
      |> assign(:loan, Loanshark.get_next_game_user_loan(game_user))
      |> assign(GameService.current(game_id))
      |> handle_socket_changes()

    GameService.subscribe(game_id)
    Games.subscribe_to_game_user(game_user.id)

    {:ok, socket}
  end

  def render(assigns), do: GameView.render("question.html", assigns)

  def handle_event("take_out_loan", _, socket) do
    {:ok, game_user, loan} =
      Loanshark.issue_loan(socket.assigns.game_user_id, socket.assigns.loan)

    socket =
      socket
      |> assign(:game_user, game_user)
      |> assign(:loan, loan)

    {:noreply, socket}
  end

  def handle_event("save", %{"game_question_user" => params}, socket) do
    res =
      GameQuestionService.answer(
        socket.assigns.game_question_id,
        socket.assigns.game_user_id,
        params["answer"],
        System.system_time(:millisecond) - socket.assigns.question_received_at
      )

    case res do
      {:ok, game_question_user} ->
        socket =
          socket
          |> remove_changeset()
          |> assign(:game_question_user, game_question_user)

        {:noreply, socket}

      {:error, changeset} ->
        socket =
          socket
          |> assign(:changeset, changeset)

        {:noreply, socket}
    end
  end

  def handle_event("save", %{"game_question_answer_game_user_bid" => params}, socket) do
    bid =
      case Integer.parse(params["bid"]) do
        {value, _} -> value
        _ -> nil
      end

    socket =
      case bid do
        nil ->
          :ok

        _ ->
          {:ok, {game_user, bid}} =
            GameQuestionService.bid(
              socket.assigns.game_question_id,
              params["game_question_answer_id"],
              socket.assigns.game_user_id,
              bid
            )

          # Assign the bid to the answer in question
          answers =
            socket.assigns.answers
            |> Enum.map(fn answer ->
              if answer.game_question_answer_id == bid.game_question_answer_id do
                answer
                |> Map.put(:bid, bid)
                |> Map.put(:changeset, nil)
              else
                answer
              end
            end)

          socket
          |> assign(:game_user, game_user)
          |> assign(:answers, answers)
      end

    {:noreply, socket}
  end

  def handle_event("update_bid", %{"game_question_answer_game_user_bid" => params}, socket) do
    bid = params["bid"]
    game_question_answer_id = Integer.parse(params["game_question_answer_id"])

    socket =
      socket
      |> assign(:answers, add_changeset_to_answer_summary(socket.assigns.answers, game_question_answer_id, bid))

    {:noreply, socket}
  end

  def handle_event("show-bid-options", game_question_answer_id, socket) do
    game_question_answer_id = String.to_integer(game_question_answer_id)

    socket =
      socket
      |> assign(:answers, add_changeset_to_answer_summary(socket.assigns.answers, game_question_answer_id))

    {:noreply, socket}
  end

  def handle_info({GameService, {:tick, _}}, socket), do: {:noreply, socket}

  def handle_info({GameService, {key, value}}, socket) do
    socket = assign(socket, key, value)
    {:noreply, handle_socket_changes(socket)}
  end

  def handle_info({Games, [:game_user, _], result}, socket) do
    {:noreply, assign(socket, :game_user, result)}
  end

  def handle_info({GameQuestionService, [:users_summary, data]}, socket) do
    {:noreply, assign(socket, :users_summary, data)}
  end

  def handle_info({GameQuestionService, [:answers_summary, data]}, socket) do
    {:noreply, assign(socket, :answers, augment_answers_summary(data, socket))}
  end

  def handle_info({GameQuestionService, [:results_summary, data]}, socket) do
    {:noreply, assign(socket, :results, data)}
  end

  def handle_info(_message, socket), do: {:noreply, socket}

  defp handle_socket_changes(socket) do
    socket
    |> handle_socket_change(:state)
    |> handle_socket_change(:game_question_id)
    |> handle_socket_change(:answers)
  end

  defp handle_socket_change(socket, key) do
    handle_socket_change(socket, key, socket.changed[key] || false)
  end

  defp handle_socket_change(socket, _, false), do: socket

  defp handle_socket_change(socket, :game_question_id, true) do
    case socket.assigns.game_question_id do
      nil ->
        socket
        |> assign(:question, nil)
        |> assign(:game_question, nil)
        |> assign(:game_question_id, nil)
        |> assign(:question_received_at, nil)
        |> assign(:bids, nil)

      game_question_id ->
        game_question = Games.get_game_question!(game_question_id) |> Repo.preload(:question)

        socket
        |> assign(:question, game_question.question)
        |> assign(:game_question, game_question)
        |> assign(:game_question_id, game_question_id)
        |> assign(:question_received_at, System.system_time(:millisecond))
        |> assign_changeset()
        |> assign_existing_answer()
    end
  end

  defp handle_socket_change(socket, :state, true) do
    case socket.assigns.state do
      "asking_question" ->
        GameQuestionService.subscribe(socket.assigns.game_question_id)

        socket
        |> assign(:users_summary, summarise_question_users(socket))

      "bidding_on_question" ->
        # TODO: Do we have to be worried about subscribing twice?
        GameQuestionService.subscribe(socket.assigns.game_question_id)

        socket
        |> assign(:answers, summarise_question_answers(socket))

      "displaying_question_results" ->
        socket
        |> assign(:results, summarise_question_results(socket))

      "ended" ->
        # TODO: We should actually terminate the genserver right now.
        socket
    end
  end

  # Iterate through the answers, finding out if the current user has placed
  # bids on any of them.
  defp handle_socket_change(socket, :answers, true) do
    socket
    |> assign(:answers, Games.Process.AddExistingBidsToAnswerSummary.for(socket.assigns.game_user_id, socket.assigns.answers))
  end

  defp remove_changeset(socket) do
    assign(socket, :changeset, nil)
  end

  defp remove_game_question_user(socket) do
    assign(socket, :game_question_user, nil)
  end

  defp assign_existing_answer(socket) do
    answer =
      Repo.get_by(GameQuestionUser,
        game_user_id: socket.assigns.game_user_id,
        game_question_id: socket.assigns.game_question_id
      )
      |> Repo.preload(game_question: :question)

    case answer do
      nil ->
        remove_game_question_user(socket)

      game_question_user ->
        assign(socket, :game_question_user, game_question_user)
    end
  end

  defp assign_changeset(socket) do
    assign(socket, :changeset, Games.change_game_question_user(%GameQuestionUser{}))
  end

  defp summarise_question_answers(socket) do
    socket.assigns.game_question_id
    |> GameQuestionService.answers_summary()
    |> augment_answers_summary(socket)
  end

  defp summarise_question_users(socket) do
    GameQuestionService.users_summary(socket.assigns.game_question_id)
  end

  defp augment_answers_summary(answers_summary, socket) do
    (answers_summary || [])
    |> Enum.map(fn answer ->
      answer
      |> Map.put(:changeset, nil)
    end)
  end

  defp add_changeset_to_answer_summary(answers_summary, game_question_answer_id, bid \\ 0) do
    Enum.map(answers_summary, fn answer ->
      if answer.game_question_answer_id == game_question_answer_id do
        changeset =
          Games.change_game_question_answer_game_user_bid(
            %Games.GameQuestionAnswerGameUserBid{},
            %{game_question_answer_id: answer.game_question_answer_id, bid: bid}
          )

        Map.put(answer, :changeset, changeset)
      else
        Map.put(answer, :changeset, nil)
      end
    end)
  end

  defp summarise_question_results(socket) do
    GameQuestionService.results_summary(socket.assigns.game_question_id)
  end
end
