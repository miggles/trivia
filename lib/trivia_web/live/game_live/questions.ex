defmodule TriviaWeb.GameLive.Questions do
  use Phoenix.LiveView

  alias Trivia.{Repo, Games, Questions}
  alias TriviaWeb.GameView

  def mount(%{game_id: game_id} = session, socket) do
    socket =
      socket
      |> assign(:game_id, game_id)

    {:ok, fetch(socket)}
  end

  def render(assigns), do: GameView.render("questions.html", assigns)

  defp fetch(socket) do
    socket
    |> assign(:game, Trivia.Games.get_game!(socket.assigns.game_id) |> Repo.preload(:questions))
  end
end
