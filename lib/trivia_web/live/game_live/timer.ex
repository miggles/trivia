defmodule TriviaWeb.GameLive.Timer do
  use Phoenix.LiveView

  alias TriviaWeb.GameView

  alias Trivia.GameService

  def mount(%{game_id: game_id, control: control} = session, socket) do
    socket =
      socket
      |> assign(:control, control)
      |> assign(:game_id, game_id)
      |> assign(GameService.current(game_id))

    GameService.subscribe(game_id)

    {:ok, socket}
  end

  def render(assigns), do: GameView.render("timer.html", assigns)

  def handle_info({GameService, {:tick, value}}, socket) do
    {:noreply, assign(socket, time_remaining: value, paused: false)}
  end

  def handle_info({GameService, {:paused, value}}, socket) do
    {:noreply, assign(socket, paused: value)}
  end

  def handle_info({GameService, {:state, value}}, socket) do
    {:noreply, assign(socket, :state, value)}
  end

  def handle_info({GameService, _}, socket) do
    {:noreply, socket}
  end

  # def handle_info({GameService, {:question, value}}, socket) do
  #   {:noreply, assign(socket, :question, value)}
  # end

  def handle_event("pause", _, socket) do
    {:ok, paused} = GameService.pause(socket.assigns.game_id, socket.assigns.state)
    {:noreply, assign(socket, :paused, paused)}
  end

  def handle_event("skip", _, socket) do
    {:ok, paused} = GameService.skip(socket.assigns.game_id, socket.assigns.state)
    {:noreply, assign(socket, :paused, paused)}
  end
end
