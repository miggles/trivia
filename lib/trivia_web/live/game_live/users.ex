defmodule TriviaWeb.GameLive.Users do
  use Phoenix.LiveView

  alias Trivia.{Accounts, Games, Repo}
  alias TriviaWeb.GameView

  alias TriviaWeb.Presence
  alias Phoenix.Socket.Broadcast

  def mount(%{game_id: game_id, user_id: user_id} = session, socket) do
    socket =
      socket
      |> assign(:game_id, game_id)
      |> assign(:user_id, user_id)
      |> add_current_user()
      |> load_game_users()

    Games.subscribe_to_game(game_id)

    Accounts.subscribe()

    Phoenix.PubSub.subscribe(Trivia.PubSub, topic(socket))

    Presence.track(self(), topic(socket), user_id, %{})

    socket =
      socket
      |> load_presence_users()

    {:ok, socket}
  end

  def render(assigns), do: GameView.render("users.html", assigns)

  def handle_info(%Broadcast{event: "presence_diff", topic: topic}, socket) do
    socket =
      case topic(socket) do
        ^topic ->
          socket
          |> load_presence_users()

        _ ->
          socket
      end

    {:noreply, socket}
  end

  def handle_info({Games, [:game_user, _], _}, socket) do
    {:noreply, load_game_users(socket)}
  end

  def handle_info({Games, [:game, _], _}, socket) do
    {:noreply, socket}
  end

  def handle_info({Accounts, [:user, :updated], user}, socket) do
    if user.id in socket.assigns.user_ids do
      {:noreply, load_game_users(socket)}
    else
      {:noreply, socket}
    end
  end

  defp topic(socket) do
    "game" <> "#{socket.assigns.game_id}"
  end

  # Find or create an entry for the current user.
  defp add_current_user(socket) do
    game_user =
      case find_game_user(socket) do
        nil ->
          game = Games.get_game!(socket.assigns.game_id)

          {:ok, game_user} =
            Games.create_game_user(%{
              game_id: socket.assigns.game_id,
              user_id: socket.assigns.user_id,
              cash: game.starting_cash
            })

          game_user

        game_user ->
          game_user
      end

    assign(socket, :game_user, game_user)
  end

  defp find_game_user(socket) do
    Repo.get_by(Games.GameUser, %{
      game_id: socket.assigns.game_id,
      user_id: socket.assigns.user_id
    })
  end

  defp load_user(socket) do
    assign(socket, :user, Accounts.get_user!(socket.assigns.user_id))
  end

  defp load_game_users(socket) do
    game =
      socket.assigns.game_id
      |> Games.get_game!()
      |> Repo.preload(:users)

    socket
    |> assign(:game, game)
    |> assign(:game_users, game.game_users)
    |> assign(:user_ids, Enum.map(game.game_users, fn gu -> gu.user_id end))
  end

  defp load_presence_users(socket) do
    user_ids =
      socket
      |> topic()
      |> Presence.list()
      |> Enum.map(fn {id, _} -> elem(Integer.parse(id), 0) end)

    socket
    |> assign(:presence_user_ids, user_ids)
  end
end
