defmodule TriviaWeb.GameLive.Show do
  use Phoenix.LiveView

  alias Trivia.{Games, GameService, Repo}
  alias TriviaWeb.GameView

  def mount(%{game_id: game_id, user_id: user_id} = session, socket) do
    socket =
      socket
      |> assign(:game_id, game_id)
      |> assign(:user_id, user_id)
      |> assign(:state, GameService.current(game_id).state)
      |> fetch()
      |> add_current_user()

    Games.subscribe_to_game(game_id)
    GameService.subscribe(game_id)

    {:ok, fetch_results(socket)}
  end

  def handle_info({GameService, {:state, value}}, socket) do
    socket = assign(socket, :state, value)
    {:noreply, fetch_results(socket)}
  end

  def handle_info({GameService, _}, socket), do: {:noreply, socket}

  def handle_info({Games, [:game, :deleted], _}, socket) do
    {:stop, redirect(socket, to: "/")}
  end

  def handle_info({Games, _, _}, socket), do: {:noreply, socket}

  def render(assigns), do: GameView.render("show.html", assigns)

  defp fetch(socket) do
    game_id = socket.assigns.game_id

    game =
      game_id
      |> Games.get_game!()
      |> Repo.preload([:user])

    socket
    |> assign(:game, game)
  end

  # Find or create an entry for the current user.
  defp add_current_user(socket) do
    game_user =
      case find_game_user(socket) do
        nil ->
          game = Games.get_game!(socket.assigns.game_id)

          {:ok, game_user} =
            Games.create_game_user(%{
              game_id: socket.assigns.game_id,
              user_id: socket.assigns.user_id,
              cash: game.starting_cash
            })

          game_user

        game_user ->
          game_user
      end

    assign(socket, :game_user_id, game_user.id)
  end

  defp find_game_user(socket) do
    Repo.get_by(Games.GameUser, %{
      game_id: socket.assigns.game_id,
      user_id: socket.assigns.user_id
    })
  end

  defp fetch_results(socket) do
    if socket.assigns.state == "ended" do
      assign(socket, :results, GameService.results(socket.assigns.game_id))
    else
      assign(socket, :results, [])
    end
  end
end
