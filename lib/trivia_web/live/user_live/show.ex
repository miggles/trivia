defmodule TriviaWeb.UserLive.Show do
  use Phoenix.LiveView
  use Phoenix.HTML

  alias TriviaWeb.UserLive
  alias TriviaWeb.Router.Helpers, as: Routes
  alias Trivia.Accounts
  alias Phoenix.LiveView.Socket

  def render(assigns) do
    ~L"""
    <h2>Show User</h2>
    <ul>
      <li><b>Username:</b> <%= @user.username %></li>
      <li><b>UUID:</b> <%= @user.uuid %></li>
    </ul>
    <span><%= link "Edit", to: Routes.live_path(@socket, UserLive.Edit, @user) %></span>
    <span><%= link "Back", to: Routes.live_path(@socket, UserLive.Index) %></span>
    """
  end

  def mount(%{path_params: %{"id" => id}}, socket) do
    if connected?(socket), do: Trivia.Accounts.subscribe(id)
    {:ok, fetch(assign(socket, id: id))}
  end

  defp fetch(%Socket{assigns: %{id: id}} = socket) do
    assign(socket, user: Accounts.get_user!(id))
  end

  def handle_info({Accounts, [:user, :updated], _}, socket) do
    {:noreply, fetch(socket)}
  end

  def handle_info({Accounts, [:user, :deleted], _}, socket) do
    {:stop,
     socket
     |> put_flash(:error, "This user has been deleted from the system")
     |> redirect(to: Routes.live_path(socket, UserLive.Index))}
  end
end
