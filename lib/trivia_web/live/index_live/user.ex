defmodule TriviaWeb.IndexLive.User do
  use Phoenix.LiveView

  alias Trivia.Accounts.User
  alias TriviaWeb.IndexView
  alias Trivia.Accounts

  def mount(%{user_id: user_id} = session, socket) do
    user = Accounts.get_user!(user_id)

    socket =
      socket
      |> assign(:user, user)
      |> assign(:changeset, User.changeset(user, %{}))

    {:ok, socket}
  end

  def render(assigns), do: IndexView.render("user.html", assigns)

  # def handle_event("edit", _params, socket) do
  #   {:noreply, assign(socket, changeset: )}
  # end

  def handle_event("validate", %{"user" => params}, socket) do
    changeset =
      socket.assigns.user
      |> User.changeset(params)
      |> Map.put(:action, :update)

    {:noreply, assign(socket, changeset: changeset)}
  end

  def handle_event("save", %{"user" => params}, socket) do
    case Accounts.update_user(socket.assigns.user, params) do
      {:ok, user} ->
        # TODO: Broadcast update - probably from Accounts
        socket =
          socket
          |> assign(:user, user)
          # |> remove_edit_flags()

        {:noreply, socket}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end

  # defp remove_edit_flags(socket) do
  #   assign(socket, :changeset, nil)
  # end
end
