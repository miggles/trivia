defmodule TriviaWeb.IndexLive.Questions do
  use Phoenix.LiveView

  alias TriviaWeb.IndexView
  alias Trivia.Questions
  alias Trivia.Questions.Question
  alias Trivia.Repo
  alias TriviaWeb.Router.Helpers, as: Routes

  def mount(%{user_id: user_id} = _session, socket) do
    Trivia.Questions.subscribe()

    socket =
      socket
      |> assign(:user_id, user_id)

    {:ok, fetch(socket)}
  end

  def render(assigns), do: IndexView.render("questions.html", assigns)

  def handle_info({Questions, [:question | _], _}, socket) do
    {:noreply, fetch(socket)}
  end

  def handle_event("show_form", _value, socket) do
    {:noreply, assign(socket, changeset: Questions.change_question(%Question{}))}
  end

  def handle_event("validate", %{"question" => params}, socket) do
    changeset =
      %Question{}
      |> Questions.change_question(params)
      |> Map.put(:action, :insert)

    {:noreply, assign(socket, changeset: changeset)}
  end

  def handle_event("save", %{"question" => params}, socket) do
    params = Map.put(params, "user_id", socket.assigns.user_id)

    case Questions.create_question(params) do
      {:ok, question} ->
        {:noreply,
         socket
         |> assign(:changeset, Questions.change_question(%Question{}))
         |> put_flash(:info, "question created")}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end

    {:noreply, socket}
  end

  def handle_event("edit", question_id, socket) do
    question =
      question_id
      |> String.to_integer()
      |> Questions.get_question!()

    {:noreply, assign(socket, changeset: Questions.change_question(question))}
  end

  def handle_event("delete", question_id, socket) do
    question_id
    |> String.to_integer()
    |> Questions.get_question!()
    |> Questions.delete_question()

    {:noreply, fetch(socket)}
  end

  defp fetch(socket) do
    assign(socket, :questions, Questions.list_questions())
  end
end
