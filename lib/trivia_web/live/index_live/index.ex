defmodule TriviaWeb.IndexLive.Index do
  use Phoenix.LiveView

  alias TriviaWeb.IndexView
  alias Trivia.Games

  # TODO: We need to subscribe to updates about this user so that if it gets
  #   edited anywhere else we reload and reassign.

  def mount(%{user_id: user_id} = _session, socket) do
    socket =
      socket
      |> assign(:user_id, user_id)

    {:ok, fetch(socket)}
  end

  def render(assigns), do: IndexView.render("index.html", assigns)

  defp fetch(socket) do
    assign(socket, :games, Games.list_games())
  end
end
