defmodule TriviaWeb.IndexLive.Games do
  use Phoenix.LiveView

  alias TriviaWeb.IndexView
  alias Trivia.Games
  alias Trivia.Games.Game
  alias Trivia.Repo
  alias TriviaWeb.Router.Helpers, as: Routes

  def mount(%{user_id: user_id} = _session, socket) do
    Trivia.Games.subscribe()

    socket =
      socket
      |> assign(:user_id, user_id)

    {:ok, fetch(socket)}
  end

  def render(assigns), do: IndexView.render("games.html", assigns)

  def handle_info({Games, [:game | _], _}, socket) do
    {:noreply, fetch(socket)}
  end

  def handle_event("show_form", _value, socket) do
    changeset =
      %Game{}
      |> Games.change_game()
      |> Ecto.Changeset.change(%{number_of_questions: 7, starting_cash: 100})

    {:noreply, assign_changeset(socket, changeset)}
  end

  def handle_event("validate", %{"game" => params}, socket) do
    changeset =
      %Game{}
      |> Games.change_game(params)
      |> Map.put(:action, :insert)

    {:noreply, assign_changeset(socket, changeset)}
  end

  defp assign_changeset(socket, changeset) do
    assign(socket,
      changeset: changeset,
      number_of_questions: changeset.changes.number_of_questions,
      starting_cash: changeset.changes.starting_cash
    )
  end

  def handle_event("save", %{"game" => params}, socket) do
    params = Map.put(params, "user_id", socket.assigns.user_id)

    case Games.create_game(params) do
      {:ok, game} ->
        # Now we need to populate the game with questions.

        {:stop,
         socket
         |> assign(:changeset, false)
         |> put_flash(:info, "game created")
         |> redirect(to: Routes.game_path(socket, :show, game.slug))}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end

    {:noreply, socket}
  end

  def handle_event("delete", game_id, socket) do
    game_id
    |> String.to_integer()
    |> Games.get_game!()
    |> Games.delete_game()

    {:noreply, fetch(socket)}
  end

  defp fetch(socket) do
    games =
      Games.list_games()
      |> Enum.sort_by(& &1.id)

    assign(socket, :games, games)
  end
end
