defmodule TriviaWeb.GameChannel do
  use Phoenix.Channel

  def join("game:" <> game_id, _message, socket) do
    {:ok, socket}
  end
end
