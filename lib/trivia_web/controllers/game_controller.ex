defmodule TriviaWeb.GameController do
  use TriviaWeb, :controller

  alias Phoenix.LiveView

  alias Trivia.{Accounts, Games.Game, Repo}

  def index(conn, _params) do
    {conn, session} = get_session(conn)

    LiveView.Controller.live_render(conn, TriviaWeb.IndexLive.Index, session: session)
  end

  def show(conn, %{"slug" => slug}) do
    {conn, session} = get_session(conn)

    game =
      case Repo.get_by(Game, %{slug: slug}) do
        nil ->
          redirect(conn, to: "/")

        game ->
          session = Map.put(session, :game_id, game.id)
          LiveView.Controller.live_render(conn, TriviaWeb.GameLive.Show, session: session)
      end
  end

  def questions(conn, _params) do
    {conn, session} = get_session(conn)

    LiveView.Controller.live_render(conn, TriviaWeb.IndexLive.Questions, session: session)
  end

  defp get_session(conn) do
    {conn, user} = set_user(conn)

    session = %{
      user_id: user.id
    }

    {conn, session}
  end

  defp set_user(conn) do
    user = find_or_create_user(conn)

    conn =
      conn
      |> put_session(:user_id, user.id)

    {conn, user}
  end

  defp find_or_create_user(conn) do
    case get_session(conn, :user_id) do
      nil ->
        {:ok, user} =
          Accounts.create_user(%{username: "guest#{System.unique_integer([:positive])}"})

        user

      id ->
        user = Accounts.get_user!(id)
        user
    end
  end
end
