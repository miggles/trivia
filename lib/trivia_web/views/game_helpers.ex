defmodule TriviaWeb.GameHelpers do
  import Phoenix.HTML
  # import Phoenix.HTML.Form
  # import Phoenix.HTML.Link
  # import Phoenix.HTML.Tag

  # TODO: This is really bad - we're not escaping the username by doing this.
  def user_bid(bid) do
    """
    <li>
      <div class="user user_#{bid.game_user.user.id} game_user game_user_#{bid.game_user.id}">
        <span class="stack stack_#{Trivia.Games.Process.Rank.get_rank(bid.bid)}"></span>
        #{bid.game_user.user.username}
      </div>
    </li>
    """
  end

  def time_remaining(nil) do
    ~e""
  end

  def time_remaining(time) do
    minutes = div(time, 60)
    seconds = rem(time, 60)

    ~e"""
    <%= minutes %>:<%= String.pad_leading(seconds |> to_string(), 2, "0") %>
    """
  end
end
