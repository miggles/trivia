defmodule TriviaWeb.Router do
  use TriviaWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug Phoenix.LiveView.Flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :put_layout, {TriviaWeb.LayoutView, :app}
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :basic_auth do
    plug BasicAuth, username: "dana", password: "bestdog"
  end

  scope "/", TriviaWeb do
    pipe_through [:browser]

    if Mix.env() == "production" do
      pipe_through [:basic_auth]
    end

    get "/", GameController, :index

    get "/game/:slug", GameController, :show

    get "/questions-no-danny", GameController, :questions

    # live "/users", UserLive.Index
    # live "/users/new", UserLive.New
    # live "/users/:id", UserLive.Show
    # live "/users/:id/edit", UserLive.Edit

    # resources "/plain/users", UserController
  end

  # Other scopes may use custom stacks.
  # scope "/api", TriviaWeb do
  #   pipe_through :api
  # end
end
