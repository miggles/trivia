defmodule Trivia.GameService.Supervisor do
  use ProcessRegistry.Supervisor, worker: Trivia.GameService
end
