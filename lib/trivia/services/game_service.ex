defmodule Trivia.GameService do
  use ProcessRegistry.Worker, supervisor: Trivia.GameService.Supervisor

  alias Trivia.{
    Games,
    Games.Summaries,
    GameQuestionService,
    Repo
  }

  @topic inspect(__MODULE__)

  # Five minute timeout on old games
  @timeout 300_000

  def subscribe(game_id) do
    find_or_create_process(game_id)
    Phoenix.PubSub.subscribe(Trivia.PubSub, @topic <> "#{game_id}")
  end

  def current(game_id) do
    GenServer.call(find_or_create_process(game_id), :current)
  end

  def pause(game_id, state) do
    GenServer.call(find_or_create_process(game_id), {:pause, state})
  end

  def skip(game_id, state) do
    GenServer.call(find_or_create_process(game_id), {:skip, state})
  end

  def results(game_id) do
    Summaries.game_results(game_id)
  end

  defmodule State do
    defstruct game_id: nil,
              game: nil,
              timer: nil,
              state: nil,
              paused: nil,
              state_ends_at: nil,
              game_question_id: nil,
              game_questions: nil,
              question_number: nil,
              question_count: nil
  end

  def init(game_id) do
    {:ok, %State{game_id: game_id}, {:continue, :setup_game_state}}
  end

  def handle_continue(:setup_game_state, %{game_id: game_id}) do
    Games.subscribe_to_game(game_id)

    game =
      game_id
      |> Games.get_game!()
      |> Repo.preload(:game_questions)

    game_questions =
      game.game_questions
      |> Enum.sort_by(& &1.id)

    game_questions =
      case game.current_game_question_id do
        nil ->
          game_questions

        current_game_question_id ->
          current_question_index =
            game_questions
            |> Enum.find_index(&(&1.id == current_game_question_id))

          Enum.slice(game_questions, current_question_index + 1, game.number_of_questions)
      end

    state = %State{
      game_id: game_id,
      game: game,
      state: game.state,
      state_ends_at: game.state_ends_at,
      game_questions: game_questions,
      game_question_id: game.current_game_question_id,
      question_count: game.number_of_questions,
      question_number: game.number_of_questions - Enum.count(game_questions),
      paused: false
    }

    # Start the clock.
    send(self(), :tick)

    {:noreply, state}
  end

  def handle_info(:tick, state), do: perform_tick(state)

  def handle_info(:timeout, state), do: {:stop, :normal, state}

  def handle_info({Games, [:game, :deleted], _}, state), do: {:stop, :normal, state}

  def handle_info({Games, _, _}, state), do: {:noreply, state}

  # TODO: Filter this down to only the fields that are relevant for the current state
  def handle_call(:current, _from, state) do
    current = %{
      state: state.state,
      game_question_id: state.game_question_id,
      question_number: state.question_number,
      question_count: state.game.number_of_questions,
      timer: state.timer,
      paused: state.paused
    }

    {:reply, current, state}
  end

  def handle_call({:pause, game_state}, _from, state) do
    case game_state == state.game.state do
      true ->
        notify_subscribers({:paused, true}, state)
        {:reply, {:ok, true}, %State{state | paused: true}}

      false ->
        notify_subscribers({:paused, false}, state)
        {:reply, {:ok, false}, state}
    end
  end

  def handle_call({:skip, game_state}, _from, state) do
    case game_state == state.game.state do
      true ->
        notify_subscribers({:paused, false}, state)
        {:ok, now} = DateTime.now("Etc/UTC")
        {:reply, {:ok, false}, %State{state | state_ends_at: now, paused: false}}

      false ->
        {:reply, {:ok, true}, state}
    end
  end

  defp perform_tick(state) do
    state = advance_state(state)

    unless state.state == "ended" do
      Process.send_after(self(), :tick, 50)
    end

    {:noreply, process_timer(state), @timeout}
  end

  defp notify_subscribers(event, %{game_id: game_id}) do
    Phoenix.PubSub.broadcast(Trivia.PubSub, @topic <> "#{game_id}", {__MODULE__, event})
  end

  # If the timer has run out we need to move to the next phase of the game and
  # reset the clock.
  defp advance_state(%{paused: true} = state), do: state

  defp advance_state(%{state: "ended"} = state), do: state

  defp advance_state(%{state_ends_at: state_ends_at} = state) do
    {:ok, now} = DateTime.now("Etc/UTC")

    case DateTime.compare(state_ends_at, now) do
      :gt ->
        state

      _ ->
        # Advance the state
        state =
          case state.state do
            "waiting_to_start" ->
              advance_to_question(state)

            "asking_question" ->
              # If we have no answers supplied, skip to displaying results
              case get_game_question_user_count(state) do
                0 -> advance_to_display(state)
                _ -> advance_to_bidding(state)
              end

            "bidding_on_question" ->
              advance_to_display(state)

            "displaying_question_results" ->
              advance_to_question(state)
          end

        # Reset the paused status and update ends at
        state = %State{
          state
          | paused: false,
            state: state.game.state,
            state_ends_at: state.game.state_ends_at
        }

        # Notify of state change
        notify_subscribers({:state, state.game.state}, state)

        # And return the state
        state
    end
  end

  # Emit the difference between now and when the state ends
  defp process_timer(%{paused: true} = state), do: state

  defp process_timer(%{state: "ended"} = state), do: state

  defp process_timer(%{state_ends_at: state_ends_at, game_id: game_id} = state) do
    {:ok, now} = DateTime.now("Etc/UTC")
    diff = DateTime.diff(state_ends_at, now)

    if state.timer != diff do
      notify_subscribers({:tick, diff}, state)

      # Broadcast to the socket subscribers, too.
      TriviaWeb.Endpoint.broadcast("game:#{game_id}", "tick", %{
        time_remaining: diff
      })

      %State{state | timer: diff}
    else
      state
    end
  end

  defp advance_to_question(
         %State{
           game: game,
           game_questions: [game_question | game_questions],
           question_number: question_number
         } = state
       ) do
    {:ok, game} =
      Games.update_game(game, %{
        state: "asking_question",
        state_ends_at: time_from_now(game.ask_duration),
        current_game_question_id: game_question.id
      })

    notify_subscribers({:game_question_id, game_question.id}, state)

    question_number = question_number + 1
    notify_subscribers({:question_number, question_number}, state)

    %State{
      state
      | game: game,
        game_questions: game_questions,
        game_question_id: game_question.id,
        question_number: question_number
    }
  end

  defp advance_to_question(%State{game: game, game_questions: []} = state) do
    {:ok, game} =
      Games.update_game(game, %{
        state: "ended",
        state_ends_at: nil,
        current_game_question_id: nil
      })

    notify_subscribers({:game_question_id, nil}, state)

    %State{state | game: game, game_question_id: nil}
  end

  defp advance_to_bidding(%{game: game, game_question_id: game_question_id} = state) do
    GameQuestionService.finalise_answers(state.game_question_id)

    {:ok, game} =
      Games.update_game(game, %{
        state: "bidding_on_question",
        state_ends_at: time_from_now(game.bid_duration)
      })

    notify_subscribers({:game_question_id, game_question_id}, state)

    %State{state | game: game}
  end

  # Just change state to displaying
  defp advance_to_display(%{game: game} = state) do
    GameQuestionService.payout(state.game_question_id)

    {:ok, game} =
      Games.update_game(game, %{
        state: "displaying_question_results",
        state_ends_at: time_from_now(game.display_duration)
      })

    %State{state | game: game}
  end

  defp get_game_question_user_count(%State{game_question_id: nil}), do: 0

  defp get_game_question_user_count(%State{game_question_id: game_question_id}) do
    Games.get_game_question_user_count(game_question_id)
  end

  defp time_from_now(seconds) do
    {:ok, now} = DateTime.now("Etc/UTC")
    DateTime.add(now, seconds, :second)
  end
end
