defmodule Trivia.GameQuestionService do
  use ProcessRegistry.Worker, supervisor: Trivia.GameQuestionService.Supervisor

  alias Trivia.{
    Games,
    Games.Summaries,
    GameService,
    Repo
  }

  @topic inspect(__MODULE__)

  # Two minute timeout on old game questions
  @timeout 120_000

  # TODO: This needs to be smarter?
  @correct_bonus 100

  def answer(game_question_id, game_user_id, answer, delay),
    do: with_ref(game_question_id, &GenServer.call(&1, {:answer, game_user_id, answer, delay}))

  def finalise_answers(game_question_id),
    do: with_ref(game_question_id, &GenServer.call(&1, :finalise_answers))

  def bid(game_question_id, game_question_answer_id, game_user_id, bid),
    do:
      with_ref(
        game_question_id,
        &GenServer.call(&1, {:bid, game_question_answer_id, game_user_id, bid})
      )

  def payout(game_question_id), do: with_ref(game_question_id, &GenServer.call(&1, :payout))

  def users_summary(game_question_id) do
    with_ref(game_question_id, fn _ ->
      :ets.lookup(ets_table_name(game_question_id), :users)[:users]
    end)
  end

  def answers_summary(game_question_id) do
    with_ref(game_question_id, fn _ ->
      :ets.lookup(ets_table_name(game_question_id), :answers)[:answers]
    end)
  end

  def bids_summary(game_question_id) do
    with_ref(game_question_id, fn _ ->
      :ets.lookup(ets_table_name(game_question_id), :bids)[:bids]
    end)
  end

  def results_summary(game_question_id) do
    with_ref(game_question_id, fn _ ->
      :ets.lookup(ets_table_name(game_question_id), :results)[:results] || []
    end)
  end

  def subscribe(game_question_id) do
    find_or_create_process(game_question_id)
    Phoenix.PubSub.subscribe(Trivia.PubSub, @topic <> "#{game_question_id}")
  end

  defmodule State do
    defstruct game_id: nil, game_question_id: nil, game_question: nil, game_state: nil
  end

  def init(game_question_id) do
    # Initialise our ets table
    :ets.new(ets_table_name(game_question_id), [
      :set,
      :protected,
      :named_table,
      read_concurrency: true
    ])

    # Get the game state as of right now, any further changes will come from
    # the subscription.
    game_question =
      game_question_id
      |> Games.get_game_question!()
      |> Repo.preload(:question)
      |> Repo.preload(:game)

    # Do not subscribe in init since that could cause a deadlock.
    send(self(), :subscribe)

    state = %State{
      game_id: game_question.game.id,
      game_question_id: game_question_id,
      game_question: game_question,
      game_state: game_question.game.state
    }

    {:ok, state, {:continue, :update_summaries}}
  end

  defp ets_table_name(game_question_id) do
    :"#{__MODULE__}.#{game_question_id}"
  end

  def handle_continue(:update_summaries, state) do
    state =
      state
      |> update_users_summary()
      |> update_answers_summary()
      |> update_bids_summary()
      |> update_results_summary()

    {:noreply, state}
  end

  def handle_call({:answer, game_user_id, answer, delay}, _from, state) do
    cond do
      state.game_state != "asking_question" ->
        {:reply, {:error, :incorrect_state}, state}

      true ->
        attrs = %{
          game_question_id: state.game_question_id,
          game_user_id: game_user_id,
          answer: answer,
          delay: delay
        }

        res = Games.create_game_question_user(attrs)

        case res do
          {:ok, _game_question_user} ->
            state
            |> update_users_summary()
            |> publish_users_summary()

            :ok

          _ ->
            :ok
        end

        {:reply, res, state}
    end
  end

  def handle_call({:bid, game_question_answer_id, game_user_id, bid}, _from, state) do
    game_user = Games.get_game_user!(game_user_id)
    game_question_answer = Games.get_game_question_answer!(game_question_answer_id)

    cond do
      state.game_state != "bidding_on_question" ->
        {:reply, {:error, :incorrect_state}, state, @timeout}

      game_user.cash < bid ->
        {:reply, {:error, :insufficient_funds}, state, @timeout}

      true ->
        res =
          Repo.transaction(fn ->
            payout =
              cond do
                game_question_answer.correct ->
                  round(bid * game_question_answer.payout_rate + @correct_bonus)

                game_question_answer.winner ->
                  round(bid * game_question_answer.payout_rate)

                true ->
                  0
              end

            bid_response =
              Games.create_game_question_answer_game_user_bid(%{
                game_question_answer_id: game_question_answer_id,
                game_user_id: game_user_id,
                bid: bid,
                payout: payout
              })

            case bid_response do
              {:error, changeset} -> Repo.rollback(changeset)
              _ -> :ok
            end

            game_user_response =
              Games.update_game_user(game_user, %{
                cash: game_user.cash - bid
              })

            case game_user_response do
              {:error, changeset} -> Repo.rollback(changeset)
              _ -> :ok
            end

            {game_user_response, bid_response}
          end)

        case res do
          {:error, _} ->
            {:reply, res, state, @timeout}

          {:ok, {{:ok, game_user}, {:ok, bid}}} ->
            state
            |> update_bids_summary()
            |> broadcast_bid(bid)

            # And return
            {:reply, {:ok, {game_user, bid}}, state, @timeout}
        end
    end
  end

  def handle_call(:payout, _from, state) do
    Games.Process.PayoutQuestion.finalise_payouts(state.game_question_id)

    state
    |> update_results_summary()
    |> publish_results_summary()

    {:reply, :ok, state}
  end

  def handle_call(:finalise_answers, _from, state) do
    Games.Process.CreateGameAnswers.for(state.game_question_id)

    state
    |> update_answers_summary()
    |> publish_answers_summary()

    {:reply, :ok, state}
  end

  def handle_info(:timeout, state), do: {:stop, :normal, state}

  def handle_info({GameService, {:state, game_state}}, state) do
    {:noreply, %{state | game_state: game_state}}
  end

  def handle_info({GameService, _}, state) do
    {:noreply, state}
  end

  def handle_info(:subscribe, %{game_id: game_id} = state) do
    GameService.subscribe(game_id)
    {:noreply, state}
  end

  defp update_users_summary(state) do
    :ets.insert(
      ets_table_name(state.game_question_id),
      {:users, Summaries.game_question_users(state.game_question_id)}
    )

    state
  end

  defp update_answers_summary(state) do
    :ets.insert(
      ets_table_name(state.game_question_id),
      {:answers, Summaries.game_question_answers(state.game_question_id)}
    )

    state
  end

  defp update_bids_summary(state) do
    :ets.insert(
      ets_table_name(state.game_question_id),
      {:bids, Summaries.game_question_answer_bids(state.game_question_id)}
    )

    state
  end

  defp update_results_summary(state) do
    :ets.insert(
      ets_table_name(state.game_question_id),
      {:results, Summaries.game_question_results(state.game_id, state.game_question_id)}
    )

    state
  end

  defp publish_users_summary(state) do
    Phoenix.PubSub.broadcast(
      Trivia.PubSub,
      @topic <> "#{state.game_question_id}",
      {__MODULE__, [:users_summary, users_summary(state.game_question_id)]}
    )
  end

  defp publish_answers_summary(state) do
    Phoenix.PubSub.broadcast(
      Trivia.PubSub,
      @topic <> "#{state.game_question_id}",
      {__MODULE__, [:answers_summary, answers_summary(state.game_question_id)]}
    )
  end

  defp publish_results_summary(state) do
    Phoenix.PubSub.broadcast(
      Trivia.PubSub,
      @topic <> "#{state.game_question_id}",
      {__MODULE__, [:results_summary, results_summary(state.game_question_id)]}
    )
  end

  defp broadcast_bid(state, bid) do
    bid = Games.get_game_question_answer_game_user_bid_with_user(bid.id)

    TriviaWeb.Endpoint.broadcast("game:#{state.game_id}", "bid_created", %{
      game_question_answer_id: bid.game_question_answer_id,
      html: TriviaWeb.GameHelpers.user_bid(bid)
    })
  end

  def terminate(_reason, %{game_question_id: game_question_id}) do
    :ets.delete(ets_table_name(game_question_id))
    :ok
  end
end
