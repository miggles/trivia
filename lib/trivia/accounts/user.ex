defmodule Trivia.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :username, :string
    field :uuid, :string

    has_many(:games, Trivia.Games.Game)
    has_many(:questions, Trivia.Questions.Question)

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:username])
    |> validate_required([:username])
    |> validate_length(:username, max: 20)
    # |> validate_format(:username, ~r/^[A-Za-zÀ-ÖØ-öø-ÿ0-9\.\,\' ]*$/,
    #   message: "only letters and spaces please"
    # )
    |> unique_constraint(:username)
  end
end
