defmodule Trivia.Questions.Question do
  use Ecto.Schema
  import Ecto.Changeset

  schema "questions" do
    field :question, :string
    field :question_html, :string
    field :unit, :string
    field :citation, :string
    field :citation_html, :string
    field :answer, :integer

    belongs_to(:user, Trivia.Accounts.User)

    has_many(:game_questions, Trivia.Games.GameQuestion)
    # has_many(:games, through: [:game_questions, :game])
    many_to_many(:questions, Trivia.Questions.Question, join_through: Trivia.Games.GameQuestion)

    timestamps()
  end

  @doc false
  def changeset(question, attrs) do
    question
    |> cast(attrs, [:question, :unit, :answer, :user_id, :citation])
    |> validate_required([:question, :unit, :answer, :citation])
    |> validate_number(:answer,
      greater_than: Application.get_env(:trivia, :minimum_value),
      less_than: Application.get_env(:trivia, :max_value)
    )
    |> validate_markdown(:question)
    |> validate_markdown(:citation)
    |> foreign_key_constraint(:user_id)
  end

  def validate_markdown(changeset, field) do
    validate_change(changeset, field, fn _, value ->
      case Earmark.as_html(value) do
        {:ok, _, _} ->
          []

        {:error, _, messages} ->
          messages
          |> Enum.map(fn {_severity, line, message} -> {field, "#{message} on line #{line}"} end)
      end
    end)
  end

  def populate_html(changeset) do
    if changeset.valid? do
      changeset
      |> populate_html(:question)
      |> populate_html(:citation)
    else
      changeset
    end
  end

  defp populate_html(changeset, field) do
    {_, markdown} = fetch_field(changeset, field)
    {:ok, html, _} = Earmark.as_html(markdown)
    put_change(changeset, :"#{field}_html", html)
  end
end
