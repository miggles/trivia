defmodule Trivia.Games do
  @moduledoc """
  The Games context.
  """

  import Ecto.Query, warn: false
  alias Trivia.Repo

  alias Trivia.Games.{
    Game,
    GameUser,
    GameUserLoan,
    GameQuestion,
    GameQuestionAnswer,
    GameQuestionAnswerGameUser,
    GameQuestionAnswerGameUserBid,
    GameQuestionUser
  }

  @topic inspect(__MODULE__)

  def subscribe do
    Phoenix.PubSub.subscribe(Trivia.PubSub, @topic)
  end

  def subscribe_to_game(game_id) do
    Phoenix.PubSub.subscribe(Trivia.PubSub, @topic <> "#{game_id}")
  end

  def subscribe_to_game_user(game_user_id) do
    Phoenix.PubSub.subscribe(Trivia.PubSub, @topic <> ":game_user:#{game_user_id}")
  end

  def subscribe_to_game_question(game_question_id) do
    Phoenix.PubSub.subscribe(Trivia.PubSub, @topic <> ":game_question:#{game_question_id}")
  end

  def notify_subscribers({:ok, %Game{} = result}, event) do
    Phoenix.PubSub.broadcast(Trivia.PubSub, @topic, {__MODULE__, event, result})
    Phoenix.PubSub.broadcast(Trivia.PubSub, @topic <> "#{result.id}", {__MODULE__, event, result})
    {:ok, result}
  end

  def notify_subscribers({:ok, %GameUser{} = result}, event) do
    # TODO: Remove this.
    Phoenix.PubSub.broadcast(
      Trivia.PubSub,
      @topic <> "#{result.game_id}",
      {__MODULE__, event, result}
    )

    Phoenix.PubSub.broadcast(
      Trivia.PubSub,
      @topic <> ":game_user:#{result.id}",
      {__MODULE__, event, result}
    )

    {:ok, result}
  end

  def notify_subscribers({:ok, %GameQuestionUser{} = result}, _event) do
    # TODO: ?
    # Phoenix.PubSub.broadcast(Trivia.PubSub, @topic <> "#{result.game_id}", {__MODULE__, event, result})
    {:ok, result}
  end

  def notify_subscribers(result, _event), do: result

  @doc """
  Returns the list of games.

  ## Examples

      iex> list_games()
      [%Game{}, ...]

  """
  def list_games do
    # Preload the creating user
    # TODO: Remove this
    query = from(m in Game, join: a in assoc(m, :user), preload: [user: a])
    Repo.all(query)
  end

  @doc """
  Gets a single game.

  Raises `Ecto.NoResultsError` if the Game does not exist.

  ## Examples

      iex> get_game!(123)
      %Game{}

      iex> get_game!(456)
      ** (Ecto.NoResultsError)

  """
  def get_game!(id), do: Repo.get!(Game, id)

  @doc """
  Creates a game.

  ## Examples

      iex> create_game(%{field: value})
      {:ok, %Game{}}

      iex> create_game(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_game(attrs \\ %{}) do
    %Game{}
    |> Game.changeset(attrs)
    |> Repo.insert()
    |> notify_subscribers([:game, :created])
  end

  @doc """
  Updates a game.

  ## Examples

      iex> update_game(game, %{field: new_value})
      {:ok, %Game{}}

      iex> update_game(game, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_game(%Game{} = game, attrs) do
    game
    |> Game.changeset(attrs)
    |> Repo.update()
    |> notify_subscribers([:game, :updated])
  end

  @doc """
  Deletes a Game.

  ## Examples

      iex> delete_game(game)
      {:ok, %Game{}}

      iex> delete_game(game)
      {:error, %Ecto.Changeset{}}

  """
  def delete_game(%Game{} = game) do
    # We have to do this in a specific order to not upset the foreign key
    # relationships.
    {:ok, res} =
      Repo.transaction(fn ->
        Ecto.Adapters.SQL.query!(
          Repo,
          "UPDATE games SET current_game_question_id = null WHERE games.id = $1",
          [game.id]
        )

        Repo.delete_all(
          from gqagub in GameQuestionAnswerGameUserBid,
            join: gqa in assoc(gqagub, :game_question_answer),
            join: gq in assoc(gqa, :game_question),
            where: gq.game_id == ^game.id
        )

        Repo.delete_all(
          from gqagu in GameQuestionAnswerGameUser,
            join: gqa in assoc(gqagu, :game_question_answer),
            join: gq in assoc(gqa, :game_question),
            where: gq.game_id == ^game.id
        )

        Repo.delete_all(
          from gqa in GameQuestionAnswer,
            join: gq in assoc(gqa, :game_question),
            where: gq.game_id == ^game.id
        )

        Repo.delete_all(
          from gul in GameUserLoan,
            join: gu in assoc(gul, :game_user),
            where: gu.game_id == ^game.id
        )

        Repo.delete_all(
          from gqu in GameQuestionUser,
            join: gu in assoc(gqu, :game_user),
            where: gu.game_id == ^game.id
        )

        Repo.delete(game)
        |> notify_subscribers([:game, :deleted])
      end)

    res
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking game changes.

  ## Examples

      iex> change_game(game)
      %Ecto.Changeset{source: %Game{}}

  """
  def change_game(%Game{} = game, attrs \\ %{}) do
    Game.changeset(game, attrs)
  end

  @doc """
  Returns the list of game_users.

  ## Examples

      iex> list_game_users()
      [%GameUser{}, ...]

  """
  def list_game_users do
    Repo.all(GameUser)
  end

  @doc """
  Gets a single game_user.

  Raises `Ecto.NoResultsError` if the Game user does not exist.

  ## Examples

      iex> get_game_user!(123)
      %GameUser{}

      iex> get_game_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_game_user!(id), do: Repo.get!(GameUser, id)

  @doc """
  Creates a game_user.

  ## Examples

      iex> create_game_user(%{field: value})
      {:ok, %GameUser{}}

      iex> create_game_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_game_user(attrs \\ %{}) do
    %GameUser{}
    |> GameUser.changeset(attrs)
    |> Repo.insert()
    |> notify_subscribers([:game_user, :created])
  end

  @doc """
  Updates a game_user.

  ## Examples

      iex> update_game_user(game_user, %{field: new_value})
      {:ok, %GameUser{}}

      iex> update_game_user(game_user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_game_user(%GameUser{} = game_user, attrs) do
    game_user
    |> GameUser.changeset(attrs)
    |> Repo.update()
    |> notify_subscribers([:game_user, :updated])
  end

  @doc """
  Deletes a GameUser.

  ## Examples

      iex> delete_game_user(game_user)
      {:ok, %GameUser{}}

      iex> delete_game_user(game_user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_game_user(%GameUser{} = game_user) do
    Repo.delete(game_user)
    |> notify_subscribers([:game_user, :deleted])
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking game_user changes.

  ## Examples

      iex> change_game_user(game_user)
      %Ecto.Changeset{source: %GameUser{}}

  """
  def change_game_user(%GameUser{} = game_user, attrs \\ %{}) do
    GameUser.changeset(game_user, attrs)
  end

  @doc """
  Returns the list of game_questions.

  ## Examples

      iex> list_game_questions()
      [%GameQuestion{}, ...]

  """
  def list_game_questions do
    Repo.all(GameQuestion)
  end

  @doc """
  Gets a single game_question.

  Raises `Ecto.NoResultsError` if the Game question does not exist.

  ## Examples

      iex> get_game_question!(123)
      %GameQuestion{}

      iex> get_game_question!(456)
      ** (Ecto.NoResultsError)

  """
  def get_game_question!(id), do: Repo.get!(GameQuestion, id)

  @doc """
  Creates a game_question.

  ## Examples

      iex> create_game_question(%{field: value})
      {:ok, %GameQuestion{}}

      iex> create_game_question(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_game_question(attrs \\ %{}) do
    %GameQuestion{}
    |> GameQuestion.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a game_question.

  ## Examples

      iex> update_game_question(game_question, %{field: new_value})
      {:ok, %GameQuestion{}}

      iex> update_game_question(game_question, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_game_question(%GameQuestion{} = game_question, attrs) do
    game_question
    |> GameQuestion.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a GameQuestion.

  ## Examples

      iex> delete_game_question(game_question)
      {:ok, %GameQuestion{}}

      iex> delete_game_question(game_question)
      {:error, %Ecto.Changeset{}}

  """
  def delete_game_question(%GameQuestion{} = game_question) do
    Repo.delete(game_question)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking game_question changes.

  ## Examples

      iex> change_game_question(game_question)
      %Ecto.Changeset{source: %GameQuestion{}}

  """
  def change_game_question(%GameQuestion{} = game_question) do
    GameQuestion.changeset(game_question, %{})
  end

  @doc """
  Returns the list of game_question_users.

  ## Examples

      iex> list_game_question_users()
      [%GameQuestionUser{}, ...]

  """
  def list_game_question_users do
    Repo.all(GameQuestionUser)
  end

  @doc """
  Gets a single game_question_user.

  Raises `Ecto.NoResultsError` if the Game question user does not exist.

  ## Examples

      iex> get_game_question_user!(123)
      %GameQuestionUser{}

      iex> get_game_question_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_game_question_user!(id), do: Repo.get!(GameQuestionUser, id)

  @doc """
  Returns a count of user answers for the supplied game question.
  """
  def get_game_question_user_count(game_question_id) do
    Repo.one(
      from gqu in "game_question_users",
        select: fragment("count(*)"),
        where: gqu.game_question_id == ^game_question_id
    )
  end

  @doc """
  Creates a game_question_user.

  ## Examples

      iex> create_game_question_user(%{field: value})
      {:ok, %GameQuestionUser{}}

      iex> create_game_question_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_game_question_user(attrs \\ %{}) do
    %GameQuestionUser{}
    |> GameQuestionUser.changeset(attrs)
    |> Repo.insert()
    |> notify_subscribers([:game_question_user, :created])
  end

  @doc """
  Updates a game_question_user.

  ## Examples

      iex> update_game_question_user(game_question_user, %{field: new_value})
      {:ok, %GameQuestionUser{}}

      iex> update_game_question_user(game_question_user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_game_question_user(%GameQuestionUser{} = game_question_user, attrs) do
    game_question_user
    |> GameQuestionUser.changeset(attrs)
    |> Repo.update()
    |> notify_subscribers([:game_question_user, :updated])
  end

  @doc """
  Deletes a GameQuestionUser.

  ## Examples

      iex> delete_game_question_user(game_question_user)
      {:ok, %GameQuestionUser{}}

      iex> delete_game_question_user(game_question_user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_game_question_user(%GameQuestionUser{} = game_question_user) do
    Repo.delete(game_question_user)
    |> notify_subscribers([:game_question_user, :deleted])
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking game_question_user changes.

  ## Examples

      iex> change_game_question_user(game_question_user)
      %Ecto.Changeset{source: %GameQuestionUser{}}

  """
  def change_game_question_user(%GameQuestionUser{} = game_question_user, attrs \\ %{}) do
    GameQuestionUser.changeset(game_question_user, attrs)
  end

  @doc """
  Returns the list of game_question_answers.

  ## Examples

      iex> list_game_question_answers()
      [%GameQuestionAnswer{}, ...]

  """
  def list_game_question_answers do
    Repo.all(GameQuestionAnswer)
  end

  @doc """
  Gets a single game_question_answer.

  Raises `Ecto.NoResultsError` if the Game question answer does not exist.

  ## Examples

      iex> get_game_question_answer!(123)
      %GameQuestionAnswer{}

      iex> get_game_question_answer!(456)
      ** (Ecto.NoResultsError)

  """
  def get_game_question_answer!(id), do: Repo.get!(GameQuestionAnswer, id)

  @doc """
  Gets a game question answer, preloaded with users.
  """
  def get_preloaded_game_question_answers(game_question_id) do
    Repo.all(
      from gqa in GameQuestionAnswer,
        join: gqagu in assoc(gqa, :game_question_answer_game_users),
        join: gu in assoc(gqagu, :game_user),
        join: u in assoc(gu, :user),
        left_join: gqagub in assoc(gqa, :game_question_answer_game_user_bids),
        left_join: qu2 in assoc(gqagub, :game_user),
        left_join: u2 in assoc(qu2, :user),
        where: gqa.game_question_id == ^game_question_id,
        preload: [
          game_question_answer_game_users: {gqagu, game_user: {gu, user: u}},
          game_question_answer_game_user_bids: {gqagub, game_user: {qu2, user: u2}}
        ]
    )
  end

  @doc """
  Creates a game_question_answer.

  ## Examples

      iex> create_game_question_answer(%{field: value})
      {:ok, %GameQuestionAnswer{}}

      iex> create_game_question_answer(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_game_question_answer(attrs \\ %{}) do
    %GameQuestionAnswer{}
    |> GameQuestionAnswer.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a game_question_answer.

  ## Examples

      iex> update_game_question_answer(game_question_answer, %{field: new_value})
      {:ok, %GameQuestionAnswer{}}

      iex> update_game_question_answer(game_question_answer, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_game_question_answer(%GameQuestionAnswer{} = game_question_answer, attrs) do
    game_question_answer
    |> GameQuestionAnswer.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a GameQuestionAnswer.

  ## Examples

      iex> delete_game_question_answer(game_question_answer)
      {:ok, %GameQuestionAnswer{}}

      iex> delete_game_question_answer(game_question_answer)
      {:error, %Ecto.Changeset{}}

  """
  def delete_game_question_answer(%GameQuestionAnswer{} = game_question_answer) do
    Repo.delete(game_question_answer)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking game_question_answer changes.

  ## Examples

      iex> change_game_question_answer(game_question_answer)
      %Ecto.Changeset{source: %GameQuestionAnswer{}}

  """
  def change_game_question_answer(%GameQuestionAnswer{} = game_question_answer) do
    GameQuestionAnswer.changeset(game_question_answer, %{})
  end

  @doc """
  Returns the list of game_question_answer_game_users.

  ## Examples

      iex> list_game_question_answer_game_users()
      [%GameQuestionAnswerGameUser{}, ...]

  """
  def list_game_question_answer_game_users do
    Repo.all(GameQuestionAnswerGameUser)
  end

  @doc """
  Gets a single game_question_answer_game_user.

  Raises `Ecto.NoResultsError` if the Game question answer game user does not exist.

  ## Examples

      iex> get_game_question_answer_game_user!(123)
      %GameQuestionAnswerGameUser{}

      iex> get_game_question_answer_game_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_game_question_answer_game_user!(id), do: Repo.get!(GameQuestionAnswerGameUser, id)

  @doc """
  Creates a game_question_answer_game_user.

  ## Examples

      iex> create_game_question_answer_game_user(%{field: value})
      {:ok, %GameQuestionAnswerGameUser{}}

      iex> create_game_question_answer_game_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_game_question_answer_game_user(attrs \\ %{}) do
    %GameQuestionAnswerGameUser{}
    |> GameQuestionAnswerGameUser.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a game_question_answer_game_user.

  ## Examples

      iex> update_game_question_answer_game_user(game_question_answer_game_user, %{field: new_value})
      {:ok, %GameQuestionAnswerGameUser{}}

      iex> update_game_question_answer_game_user(game_question_answer_game_user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_game_question_answer_game_user(
        %GameQuestionAnswerGameUser{} = game_question_answer_game_user,
        attrs
      ) do
    game_question_answer_game_user
    |> GameQuestionAnswerGameUser.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a GameQuestionAnswerGameUser.

  ## Examples

      iex> delete_game_question_answer_game_user(game_question_answer_game_user)
      {:ok, %GameQuestionAnswerGameUser{}}

      iex> delete_game_question_answer_game_user(game_question_answer_game_user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_game_question_answer_game_user(
        %GameQuestionAnswerGameUser{} = game_question_answer_game_user
      ) do
    Repo.delete(game_question_answer_game_user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking game_question_answer_game_user changes.

  ## Examples

      iex> change_game_question_answer_game_user(game_question_answer_game_user)
      %Ecto.Changeset{source: %GameQuestionAnswerGameUser{}}

  """
  def change_game_question_answer_game_user(
        %GameQuestionAnswerGameUser{} = game_question_answer_game_user
      ) do
    GameQuestionAnswerGameUser.changeset(game_question_answer_game_user, %{})
  end

  @doc """
  Returns the list of game_question_answer_game_user_bids.

  ## Examples

      iex> list_game_question_answer_game_user_bids()
      [%GameQuestionAnswerGameUserBid{}, ...]

  """
  def list_game_question_answer_game_user_bids do
    Repo.all(GameQuestionAnswerGameUserBid)
  end

  @doc """
  Gets a single game_question_answer_game_user_bid.

  Raises `Ecto.NoResultsError` if the Game question answer game user bid does not exist.

  ## Examples

      iex> get_game_question_answer_game_user_bid!(123)
      %GameQuestionAnswerGameUserBid{}

      iex> get_game_question_answer_game_user_bid!(456)
      ** (Ecto.NoResultsError)

  """
  def get_game_question_answer_game_user_bid!(id),
    do: Repo.get!(GameQuestionAnswerGameUserBid, id)

  def get_game_question_answer_game_user_bid_with_user(id) do
    Repo.one(
      from gqagub in GameQuestionAnswerGameUserBid,
        join: gu in assoc(gqagub, :game_user),
        join: u in assoc(gu, :user),
        where: gqagub.id == ^id,
        preload: [
          game_user: {gu, user: u}
        ]
    )
  end

  def get_bids_to_pay_out(game_question_id) do
    Repo.all(
      from gqa in GameQuestionAnswer,
        join: gqagub in assoc(gqa, :game_question_answer_game_user_bids),
        join: qu2 in assoc(gqagub, :game_user),
        where: gqa.game_question_id == ^game_question_id,
        where: gqagub.paid == false,
        preload: [
          game_question_answer_game_user_bids: {gqagub, game_user: qu2}
        ]
    )
  end

  def get_bonuses_to_payout(game_question_id) do
    Repo.all(
      from gqa in GameQuestionAnswer,
        join: gqagu in assoc(gqa, :game_question_answer_game_users),
        where: gqa.game_question_id == ^game_question_id,
        preload: [
          game_question_answer_game_users: gqagu
        ]
    )
  end

  @doc """
  Creates a game_question_answer_game_user_bid.

  ## Examples

      iex> create_game_question_answer_game_user_bid(%{field: value})
      {:ok, %GameQuestionAnswerGameUserBid{}}

      iex> create_game_question_answer_game_user_bid(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_game_question_answer_game_user_bid(attrs \\ %{}) do
    %GameQuestionAnswerGameUserBid{}
    |> GameQuestionAnswerGameUserBid.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a game_question_answer_game_user_bid.

  ## Examples

      iex> update_game_question_answer_game_user_bid(game_question_answer_game_user_bid, %{field: new_value})
      {:ok, %GameQuestionAnswerGameUserBid{}}

      iex> update_game_question_answer_game_user_bid(game_question_answer_game_user_bid, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_game_question_answer_game_user_bid(
        %GameQuestionAnswerGameUserBid{} = game_question_answer_game_user_bid,
        attrs
      ) do
    game_question_answer_game_user_bid
    |> GameQuestionAnswerGameUserBid.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a GameQuestionAnswerGameUserBid.

  ## Examples

      iex> delete_game_question_answer_game_user_bid(game_question_answer_game_user_bid)
      {:ok, %GameQuestionAnswerGameUserBid{}}

      iex> delete_game_question_answer_game_user_bid(game_question_answer_game_user_bid)
      {:error, %Ecto.Changeset{}}

  """
  def delete_game_question_answer_game_user_bid(
        %GameQuestionAnswerGameUserBid{} = game_question_answer_game_user_bid
      ) do
    Repo.delete(game_question_answer_game_user_bid)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking game_question_answer_game_user_bid changes.

  ## Examples

      iex> change_game_question_answer_game_user_bid(game_question_answer_game_user_bid)
      %Ecto.Changeset{source: %GameQuestionAnswerGameUserBid{}}

  """
  def change_game_question_answer_game_user_bid(
        %GameQuestionAnswerGameUserBid{} = game_question_answer_game_user_bid,
        attrs \\ %{}
      ) do
    GameQuestionAnswerGameUserBid.changeset(game_question_answer_game_user_bid, attrs)
  end

  @doc """
  Returns the list of game_user_loans.

  ## Examples

      iex> list_game_user_loans()
      [%GameUserLoan{}, ...]

  """
  def list_game_user_loans do
    Repo.all(GameUserLoan)
  end

  @doc """
  Gets a single game_user_loan.

  Raises `Ecto.NoResultsError` if the Game user loan does not exist.

  ## Examples

      iex> get_game_user_loan!(123)
      %GameUserLoan{}

      iex> get_game_user_loan!(456)
      ** (Ecto.NoResultsError)

  """
  def get_game_user_loan!(id), do: Repo.get!(GameUserLoan, id)

  @doc """
  Creates a game_user_loan.

  ## Examples

      iex> create_game_user_loan(%{field: value})
      {:ok, %GameUserLoan{}}

      iex> create_game_user_loan(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_game_user_loan(attrs \\ %{}) do
    %GameUserLoan{}
    |> GameUserLoan.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a game_user_loan.

  ## Examples

      iex> update_game_user_loan(game_user_loan, %{field: new_value})
      {:ok, %GameUserLoan{}}

      iex> update_game_user_loan(game_user_loan, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_game_user_loan(%GameUserLoan{} = game_user_loan, attrs) do
    game_user_loan
    |> GameUserLoan.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a GameUserLoan.

  ## Examples

      iex> delete_game_user_loan(game_user_loan)
      {:ok, %GameUserLoan{}}

      iex> delete_game_user_loan(game_user_loan)
      {:error, %Ecto.Changeset{}}

  """
  def delete_game_user_loan(%GameUserLoan{} = game_user_loan) do
    Repo.delete(game_user_loan)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking game_user_loan changes.

  ## Examples

      iex> change_game_user_loan(game_user_loan)
      %Ecto.Changeset{source: %GameUserLoan{}}

  """
  def change_game_user_loan(%GameUserLoan{} = game_user_loan, attrs \\ %{}) do
    GameUserLoan.changeset(game_user_loan, attrs)
  end
end
