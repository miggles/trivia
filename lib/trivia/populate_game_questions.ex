defmodule Trivia.PopulateGameQuestions do
  import Ecto.Query, warn: false

  alias Trivia.{Questions.Question, Games.Game, Repo}

  # def for_game(%Ecto.Changeset{} = changeset) do
  #   Ecto.Changeset.put_assoc(:questions, get_random_questions)
  # end

  @doc """
  Returns a list of random questions.

  ## Examples

      iex> get_random(1)
      [%Question{}]

      iex> get_random(0)
      []

  """
  def get_random(count, excluding \\ []) do
    # Get the maximum id in the set
    [max_id] = Repo.all(from q in Question, select: max(q.id))
    get_random_questions(count, max_id, excluding)
  end

  defp get_random_questions(_, 0, _), do: []

  defp get_random_questions(0, _, _), do: []

  defp get_random_questions(count, max_id, excluding) do
    case get_random_question(max_id, excluding) do
      nil ->
        get_random_questions(count - 1, max_id, excluding)

      {question, excluding} ->
        [question | get_random_questions(count - 1, max_id, [question.id | excluding])]
    end
  end

  defp get_random_question(max_id, excluding, attempts \\ 3)

  defp get_random_question(max_id, excluding, 0), do: nil

  defp get_random_question(max_id, excluding, attempts) do
    id = get_random_number_upto(max_id, excluding)

    excluding = [id | excluding]

    case Repo.get(Question, id) do
      nil -> get_random_question(max_id, excluding, attempts - 1)
      question -> {question, excluding}
    end
  end

  defp get_random_number_upto(n, excluding \\ []) do
    i = :rand.uniform(n)

    if i in excluding do
      get_random_number_upto(n, excluding)
    else
      i
    end
  end
end
