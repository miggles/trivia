defmodule Trivia.Questions do
  @moduledoc """
  The Questions context.
  """

  import Ecto.Query, warn: false
  alias Trivia.Repo

  @topic inspect(__MODULE__)

  def subscribe do
    Phoenix.PubSub.subscribe(Trivia.PubSub, @topic)
  end

  alias Trivia.Questions.Question

  @doc """
  Returns the list of questions.

  ## Examples

      iex> list_questions()
      [%Question{}, ...]

  """
  def list_questions do
    # Repo.all(Question)

    # Preload users
    # TODO: Remove this
    query = from(m in Question, left_join: a in assoc(m, :user), preload: [user: a])
    Repo.all(query)
  end

  @doc """
  Gets a single question.

  Raises `Ecto.NoResultsError` if the Question does not exist.

  ## Examples

      iex> get_question!(123)
      %Question{}

      iex> get_question!(456)
      ** (Ecto.NoResultsError)

  """
  def get_question!(id), do: Repo.get!(Question, id)

  @doc """
  Returns `count` random questions with no other specification.
  """
  def get_random_questions(count) do
    ids = Repo.all(from q in Question, select: q.id)

    random_ids =
      ids
      |> Enum.shuffle()
      |> Enum.slice(0, count)

    Repo.all(from q in Question, where: q.id in ^random_ids)
    |> Enum.shuffle()
  end

  @doc """
  Creates a question.

  ## Examples

      iex> create_question(%{field: value})
      {:ok, %Question{}}

      iex> create_question(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_question(attrs \\ %{}) do
    %Question{}
    |> Question.changeset(attrs)
    |> Question.populate_html()
    |> Repo.insert()
    |> notify_subscribers([:question, :created])
  end

  @doc """
  Updates a question.

  ## Examples

      iex> update_question(question, %{field: new_value})
      {:ok, %Question{}}

      iex> update_question(question, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_question(%Question{} = question, attrs) do
    question
    |> Question.changeset(attrs)
    |> Question.populate_html()
    |> Repo.update()
    |> notify_subscribers([:question, :updated])
  end

  @doc """
  Deletes a Question.

  ## Examples

      iex> delete_question(question)
      {:ok, %Question{}}

      iex> delete_question(question)
      {:error, %Ecto.Changeset{}}

  """
  def delete_question(%Question{} = question) do
    Repo.delete(question)
    |> notify_subscribers([:question, :deleted])
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking question changes.

  ## Examples

      iex> change_question(question)
      %Ecto.Changeset{source: %Question{}}

  """
  def change_question(%Question{} = question, attrs \\ %{}) do
    Question.changeset(question, attrs)
  end

  defp notify_subscribers({:ok, %Question{} = result}, event) do
    Phoenix.PubSub.broadcast(Trivia.PubSub, @topic, {__MODULE__, event, result})
    Phoenix.PubSub.broadcast(Trivia.PubSub, @topic <> "#{result.id}", {__MODULE__, event, result})
    {:ok, result}
  end

  defp notify_subscribers(result, _event), do: result
end
