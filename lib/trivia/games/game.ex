defmodule Trivia.Games.Game do
  use Ecto.Schema
  import Ecto.Changeset

  alias GfycatidEx, as: Gfycat

  @states [
    "waiting_to_start",
    "asking_question",
    "bidding_on_question",
    "displaying_question_results",
    "ended"
  ]
  @question_states [
    "asking_question",
    "bidding_on_question",
    "displaying_question_results"
  ]
  @initial_state "waiting_to_start"

  # Default durations applied to all new games.
  @durations %{
    wait_duration: 180,
    ask_duration: 30,
    bid_duration: 30,
    display_duration: 30
  }

  schema "games" do
    field :slug, :string, null: false
    field :starting_cash, :integer, null: false
    field :number_of_questions, :integer, null: false

    # State handling
    field :state, :string, null: false
    field :state_ends_at, :utc_datetime

    # Durations
    field :wait_duration, :integer, null: false
    field :ask_duration, :integer, null: false
    field :bid_duration, :integer, null: false
    field :display_duration, :integer, null: false

    # User who created the game
    belongs_to(:user, Trivia.Accounts.User)

    # Question that we're currently answering
    belongs_to(:current_game_question, Trivia.Games.GameQuestion)

    # Users who are participating in the game
    has_many(:game_users, Trivia.Games.GameUser, on_delete: :delete_all)
    has_many(:users, through: [:game_users, :user])

    # Questions in the game
    has_many(:game_questions, Trivia.Games.GameQuestion, on_delete: :delete_all)
    # has_many(:questions, through: [:game_questions, :question])
    many_to_many(:questions, Trivia.Questions.Question, join_through: Trivia.Games.GameQuestion)

    timestamps()
  end

  @doc false
  def changeset(game, attrs) do
    game
    |> cast(attrs, [
      :user_id,
      :slug,
      :starting_cash,
      :number_of_questions,
      :wait_duration,
      :ask_duration,
      :bid_duration,
      :display_duration,
      :state,
      :current_game_question_id
    ])
    |> set_default_slug()
    |> set_default_durations()
    |> set_default_state()
    |> validate_required([
      :state,
      :user_id,
      :slug,
      :starting_cash,
      :number_of_questions,
      :wait_duration,
      :ask_duration,
      :bid_duration,
      :display_duration
    ])
    |> validate_number(:starting_cash, less_than_or_equal_to: 1000, greater_than_or_equal_to: 10)
    |> validate_number(:number_of_questions, less_than_or_equal_to: 10, greater_than: 0)
    |> validate_state()
    |> set_state_ends_at()
    |> validate_state_ends_at()
    |> validate_current_game_question_id()
    |> set_questions()
  end

  defp validate_state(changeset) do
    validate_change(changeset, :state, fn _, state ->
      if Enum.any?(@states, &(&1 == state)) do
        []
      else
        [{:state, "not a valid state"}]
      end
    end)
  end

  defp validate_state_ends_at(changeset) do
    validate_change(changeset, :state_ends_at, fn _, state_ends_at ->
      must_be_set = fetch_field(changeset, :state) == "ended"

      cond do
        must_be_set && (state_ends_at == nil) ->
          [state_ends_at: "must be set"]

          !must_be_set && (state_ends_at != nil)
          [state_ends_at: "must be nil"]

        true ->
          []
      end
    end)
  end

  defp validate_current_game_question_id(changeset) do
    validate_change(changeset, :current_game_question_id, fn _, current_game_question_id ->
      state = fetch_field(changeset, :state)
      must_be_set = Enum.any?(@question_states, &(&1 == state))

      cond do
        must_be_set && (current_game_question_id == nil) ->
          [current_game_question_id: "must be set"]

          !must_be_set && (current_game_question_id != nil)
          [current_game_question_id: "must be nil"]

        true ->
          []
      end
    end)
  end

  defp set_state_ends_at(changeset) do
    case get_change(changeset, :state) do
      nil ->
        changeset

      state ->
        case state do
          "waiting_to_start" ->
            put_change(changeset, :state_ends_at, get_ends_at(changeset, :wait_duration))

          "asking_question" ->
            put_change(changeset, :state_ends_at, get_ends_at(changeset, :ask_duration))

          "bidding_on_question" ->
            put_change(changeset, :state_ends_at, get_ends_at(changeset, :bid_duration))

          "displaying_question_results" ->
            put_change(changeset, :state_ends_at, get_ends_at(changeset, :display_duration))

          "ended" ->
            put_change(changeset, :state_ends_at, nil)
        end
    end
  end

  defp get_ends_at(changeset, name) do
    {:ok, now} = DateTime.now("Etc/UTC")

    now
    |> DateTime.add(get_field(changeset, name))
    |> DateTime.truncate(:second)
  end

  defp set_default_slug(changeset) do
    if get_field(changeset, :slug) == nil do
      force_change(changeset, :slug, Gfycat.generate())
    else
      changeset
    end
  end

  defp set_default_state(changeset) do
    case get_field(changeset, :state) do
      nil -> put_change(changeset, :state, @initial_state)
      _ -> changeset
    end
  end

  defp set_default_durations(changeset) do
    changeset
    |> set_default_duration(:wait_duration)
    |> set_default_duration(:ask_duration)
    |> set_default_duration(:bid_duration)
    |> set_default_duration(:display_duration)
  end

  defp set_default_duration(changeset, name) do
    case get_field(changeset, name) do
      nil -> put_change(changeset, name, @durations[name])
      _ -> changeset
    end
  end

  defp set_questions(changeset) do
    case changeset.data.id do
      nil ->
        case changeset.valid? do
          true ->
            changeset
            |> put_assoc(
              :questions,
              Trivia.Questions.get_random_questions(changeset.changes.number_of_questions)
            )

          _ ->
            changeset
        end

      _ ->
        changeset
    end
  end
end
