defmodule Trivia.Games.Process.CreateGameAnswers do
  alias Trivia.{Games, Repo}

  # How much do we pay out for answers
  @middle_payout {2, "2:1"}
  @payouts [
    {2.5, "5:2"},
    {3, "3:1"},
    {3.5, "7:2"},
    {4, "4:1"},
    {4.5, "9:2"},
    {5, "5:1"}
  ]

  # TODO: Make these configurable per game? Or remove some of that game
  #   configuration entirely?
  @winning_answer_bonus 25
  @correct_answer_bonus 50

  def for(game_question_id) when is_integer(game_question_id) do
    __MODULE__.for(get_game_question(game_question_id))
  end

  def for(%Games.GameQuestion{} = game_question) do
    user_answers =
      game_question
      |> get_user_answers()

    case Enum.count(user_answers) do
      0 ->
        {:ok, []}

      n ->
        game_question_answers =
          user_answers
          # |> IO.inspect
          |> get_answer_groups()
          |> set_winning_answer(game_question)
          |> shrink_to_playable()
          |> assign_payouts()
          # |> IO.inspect
          |> create_game_question_answers(game_question)

        {:ok, game_question_answers}
    end
  end

  def get_game_question(game_question_id) do
    game_question_id
    |> Games.get_game_question!()
    |> Repo.preload([:question, :game_question_users])
  end

  def get_user_answers(game_question) do
    Repo.preload(game_question, :game_question_users).game_question_users
  end

  def get_answer_groups(game_question_users) do
    Enum.reduce(game_question_users, %{}, fn gqu, acc ->
      case Map.get(acc, gqu.answer) do
        nil ->
          # Insert new
          Map.put(acc, gqu.answer, %{
            answer: gqu.answer,
            game_question_id: gqu.game_question_id,
            game_user_ids: [gqu.game_user_id],
            correct: false,
            winner: false
          })

        _ ->
          # Add to existing
          {_, res} =
            get_and_update_in(acc[gqu.answer][:game_user_ids], &{&1, [gqu.game_user_id | &1]})

          res
      end
    end)
  end

  # Find and mark the winning and correct answers
  #
  # Winning is the closest without going over.
  # Correct is if it's also the same as the actual answer.
  def set_winning_answer(answer_groups, game_question) do
    question = game_question.question

    answer =
      answer_groups
      |> Map.keys()
      |> Enum.sort()
      |> Enum.reverse()
      |> Enum.reject(fn v -> v > question.answer end)
      |> List.first()

    # IO.inspect {:set_winning_answer, answer, answer_groups[answer]}

    # Now tag that answer in the groups
    case answer do
      nil ->
        answer_groups

      _ ->
        answer_groups = put_in(answer_groups[answer][:winner], true)
        answer_groups = put_in(answer_groups[answer][:correct], answer == question.answer)
        answer_groups
    end
  end

  # If we've got 100 people playing, 100 answers is too many to show. This
  # function attempts to filter out random not-winning answers, preferring to
  # keep answers with multiple submitters.
  #
  # In an ideal world there would be some sort of answer clustering thinking
  # involved as well, I just don't know what that would be right now.
  #
  # There is a tell in here at the moment - if it removes all the one-user
  # except for one, that answer is probably the winning answer.
  def shrink_to_playable(answer_groups) do
    to_remove = Enum.count(answer_groups) - Application.get_env(:trivia, :maximum_answers)

    if to_remove > 0 do
      removable =
        answer_groups
        |> Enum.map(fn {k, v} ->
          cond do
            v[:winner] -> nil
            Enum.count(v[:game_user_ids]) > 1 -> nil
            true -> k
          end
        end)
        |> Enum.reject(fn v -> v == nil end)
        |> Enum.shuffle()
        |> Enum.slice(1..to_remove)


      answer_groups
      |> Enum.reject(fn {k, v} -> k in removable end)
      |> Enum.into(%{})
    else
      answer_groups
    end
  end

  # Now we need to bin the answers for payout.
  #
  # The logic for binning payouts is a bit tricksy but it goes something like this:
  #
  # The median answer gets paid out at 2:1
  # The further away from the median we are the more the payout increases:
  #   2:1 |  5:2    3:1   7:2    4:1   9:2    5:1
  #   2x  |  2.5x   3x    3.5x   4x    4.5x   5x
  #
  # However, if we don't have a wide enough set of bins we'll only use the
  # inner values.
  def assign_payouts(answer_groups) do
    spread = Map.keys(answer_groups) |> Enum.reverse()

    # IO.inspect {:spread, List.to_tuple(spread)}

    # Find our middle answer
    middle_answer = Enum.at(spread, round(Enum.count(spread) / 2 - 1))
    middle_position = Enum.find_index(spread, &(&1 == middle_answer))

    case Enum.count(answer_groups) do
      # Do nothing
      0 ->
        answer_groups

      1 ->
        middle_answer = Enum.at(Map.keys(answer_groups), 0)
        put_in(answer_groups[middle_answer][:payout], @middle_payout)

      2 ->
        [high, low] = spread
        answer_groups = put_in(answer_groups[low][:payout], @middle_payout)
        answer_groups = put_in(answer_groups[high][:payout], Enum.at(@payouts, 0))

      _many ->
        # Break our answers into three lists
        above = Enum.slice(spread, 0..(middle_position - 1))
        middle = Enum.slice(spread, middle_position..middle_position)
        below = Enum.slice(spread, (middle_position + 1)..-1)

        # IO.inspect {List.to_tuple(above), List.to_tuple(middle), List.to_tuple(below)}, charlists_as: :lists

        payouts_above = Enum.take(@payouts, min(Enum.count(above), Enum.count(@payouts)))
        payouts_below = Enum.take(@payouts, min(Enum.count(below), Enum.count(@payouts)))

        # Distribute the payouts above the middle answer
        {_, answer_groups} =
          above
          |> Enum.reverse()
          |> Enum.reduce({0, answer_groups}, fn answer, {i, answer_groups} ->
            # Get the relative position of the answer in the spread.
            pos = min(round(i / Enum.count(above) * Enum.count(payouts_above)), Enum.count(payouts_above) - 1)
            payout = Enum.at(@payouts, pos)

            # Now assign it
            {i + 1, put_in(answer_groups[answer][:payout], payout)}
          end)

        # Distribute the payouts below the middle answer
        {_, answer_groups} =
          below
          |> Enum.reduce({0, answer_groups}, fn answer, {i, answer_groups} ->
            # Get the relative position of the answer in the spread.
            pos = min(round(i / Enum.count(below) * Enum.count(payouts_below)), Enum.count(payouts_below) - 1)
            payout = Enum.at(@payouts, pos)

            # Now assign it
            {i + 1, put_in(answer_groups[answer][:payout], payout)}
          end)

        # Assign the payout on the middle answer
        put_in(answer_groups[middle_answer][:payout], @middle_payout)
    end
  end

  def create_game_question_answers(answer_groups, game_question) do
    {:ok, res} =
      Repo.transaction(fn ->
        answer_groups
        |> Map.values()
        |> Enum.sort_by(& &1[:answer])
        |> Enum.reverse()
        |> Enum.map(fn answer_group ->
          Games.create_game_question_answer(%{
            game_question_id: game_question.id,
            answer: answer_group[:answer],
            correct: answer_group[:correct],
            winner: answer_group[:winner],
            payout_rate: elem(answer_group[:payout], 0),
            payout_string: elem(answer_group[:payout], 1),
            game_question_answer_game_users:
              Enum.map(
                answer_group[:game_user_ids],
                &set_bonus_payout_rates(answer_group, %{game_user_id: &1})
              )
          })
        end)
      end)

    Enum.map(res, &elem(&1, 1))
  end

  defp assign_payout(answer_groups, answer, {payout, payout_string}) do
    answer_groups = put_in(answer_groups[answer][:payout], payout)
    answer_groups = put_in(answer_groups[answer][:payout_string], payout_string)
    answer_groups
  end

  defp set_bonus_payout_rates(%{correct: true, payout: payout}, user) do
    Map.put(user, :correct_answer_payout, round(@correct_answer_bonus * elem(payout, 0)))
  end

  defp set_bonus_payout_rates(%{winner: true, payout: payout}, user) do
    Map.put(user, :winning_answer_payout, round(@winning_answer_bonus * elem(payout, 0)))
  end

  defp set_bonus_payout_rates(_, user), do: user
end

# Trivia.CreateGameAnswers.for(257)
# Trivia.Repo.delete_all Trivia.Games.GameQuestionAnswer
