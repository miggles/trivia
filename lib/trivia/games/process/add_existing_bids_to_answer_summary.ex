defmodule Trivia.Games.Process.AddExistingBidsToAnswerSummary do
  alias Trivia.Repo
  alias Trivia.Games.GameQuestionAnswerGameUserBid

  import Ecto.Query

  # TODO: Use a single query to get all answers and then map them in.
  # TODO: Add a unique index on bids so we can Repo.one instead of getting the
  #   first of potentially many.
  def for(game_user_id, answers) do
    Enum.map(answers, fn answer ->
      query = from bid in GameQuestionAnswerGameUserBid, where: bid.game_user_id == ^game_user_id, where: bid.game_question_answer_id == ^answer.game_question_answer_id

      bid = List.first(Repo.all(query))

      Map.put(answer, :bid, bid)
    end)
  end
end
