defmodule Trivia.Games.Process.PayoutQuestion do
  import Ecto.Query

  alias Trivia.{
    Games,
    Repo
  }

  def finalise_payouts(game_question_id) do
    Repo.transaction(fn ->
      game_question_id
      |> Games.get_bids_to_pay_out()
      |> Enum.each(fn game_question_answer ->
        Enum.each(game_question_answer.game_question_answer_game_user_bids, fn bid ->
          if bid.payout > 0 do
            # Update the game user
            Ecto.Adapters.SQL.query!(
              Repo,
              "UPDATE game_users SET cash = cash + $1 WHERE id = $2",
              [bid.payout, bid.game_user_id]
            )
          end

          # Mark as paid
          Games.update_game_question_answer(game_question_answer, %{paid: true})
        end)
      end)

      game_question_id
      |> Games.get_bonuses_to_payout()
      |> Enum.each(fn game_question_answer ->
        Enum.each(game_question_answer.game_question_answer_game_users, fn user ->
          if user.winning_answer_payout > 0 do
            # Update the game user
            Ecto.Adapters.SQL.query!(
              Repo,
              "UPDATE game_users SET cash = cash + $1 WHERE id = $2",
              [user.winning_answer_payout, user.game_user_id]
            )
          end

          if user.correct_answer_payout > 0 do
            # Update the game user
            Ecto.Adapters.SQL.query!(
              Repo,
              "UPDATE game_users SET cash = cash + $1 WHERE id = $2",
              [user.correct_answer_payout, user.game_user_id]
            )
          end
        end)
      end)
    end)

    # Now notify each of the game users that they have been updated
    game_question = Games.get_game_question!(game_question_id)

    Repo.all(from gu in Games.GameUser, where: gu.game_id == ^game_question.game_id)
    |> Enum.each(fn game_user ->
      Games.notify_subscribers({:ok, game_user}, [:game_user, :updated])
    end)
  end
end
