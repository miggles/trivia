defmodule Trivia.Games.Process.Rank do
  def get_rank(amount) when amount in 1..99, do: 1
  def get_rank(amount) when amount in 100..249, do: 2
  def get_rank(amount) when amount in 250..499, do: 3
  def get_rank(amount) when amount in 500..749, do: 4
  def get_rank(amount) when amount in 750..999, do: 5
  def get_rank(amount) when amount in 1000..1499, do: 6
  def get_rank(amount) when amount in 1500..1999, do: 7
  def get_rank(amount) when amount in 2000..2499, do: 8
  def get_rank(amount) when amount > 2500, do: 9
  def get_rank(_), do: 0
end
