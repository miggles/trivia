defmodule Trivia.Games.GameUser do
  use Ecto.Schema
  import Ecto.Changeset

  schema "game_users" do
    field :cash, :integer, null: false
    field :loans_owing, :integer, null: false, default: 0

    belongs_to(:user, Trivia.Accounts.User)
    belongs_to(:game, Trivia.Games.Game)

    has_many(:game_question_users, Trivia.Games.GameQuestionUser, on_delete: :delete_all)

    has_many(:game_user_loans, Trivia.Games.GameUserLoan, on_delete: :delete_all)

    has_many(:game_question_answer_game_users, Trivia.Games.GameQuestionAnswerGameUser,
      on_delete: :delete_all
    )

    has_many(:game_question_answer_game_user_bids, Trivia.Games.GameQuestionAnswerGameUserBid,
      on_delete: :delete_all
    )

    timestamps()
  end

  @doc false
  def changeset(game_user, attrs) do
    game_user
    |> cast(attrs, [:user_id, :game_id, :cash, :loans_owing])
    |> validate_required([:user_id, :game_id, :cash, :loans_owing])
    |> validate_number(:cash, greater_than_or_equal_to: 0)
    |> validate_number(:loans_owing, greater_than_or_equal_to: 0)
  end
end
