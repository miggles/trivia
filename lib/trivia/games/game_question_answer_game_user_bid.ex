defmodule Trivia.Games.GameQuestionAnswerGameUserBid do
  use Ecto.Schema
  import Ecto.Changeset

  schema "game_question_answer_game_user_bids" do
    field :bid, :integer, null: false
    field :payout, :integer, null: false
    field :paid, :boolean, default: false

    belongs_to(:game_question_answer, Trivia.Games.GameQuestionAnswer)
    belongs_to(:game_user, Trivia.Games.GameUser)

    timestamps()
  end

  @doc false
  def changeset(game_question_answer_game_user_bid, attrs) do
    game_question_answer_game_user_bid
    |> cast(attrs, [:bid, :payout, :game_question_answer_id, :game_user_id, :paid])
    |> validate_required([:bid, :payout, :game_question_answer_id, :game_user_id])
    |> validate_number(:bid, greater_than: 0)
    |> validate_number(:payout, greater_than_or_equal_to: 0)
    |> validate_bid()
  end

  defp validate_bid(changeset) do
    # TODO: Ensure that the bid is greater than or equal to the game user cash
    changeset
  end
end
