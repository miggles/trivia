defmodule Trivia.Games.GameQuestionAnswerGameUser do
  use Ecto.Schema
  import Ecto.Changeset

  schema "game_question_answer_game_users" do
    field :winning_answer_payout, :integer, null: false, default: 0
    field :correct_answer_payout, :integer, null: false, default: 0

    belongs_to(:game_user, Trivia.Games.GameUser)
    belongs_to(:game_question_answer, Trivia.Games.GameQuestionAnswer)

    timestamps()
  end

  @doc false
  def changeset(game_question_answer_game_user, attrs) do
    game_question_answer_game_user
    |> cast(attrs, [
      :game_user_id,
      :game_question_answer_id,
      :winning_answer_payout,
      :correct_answer_payout
    ])
    |> validate_required([:game_user_id])
    |> validate_number(:winning_answer_payout, greater_than_or_equal_to: 0)
    |> validate_number(:correct_answer_payout, greater_than_or_equal_to: 0)
  end
end
