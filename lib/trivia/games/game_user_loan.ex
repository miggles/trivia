defmodule Trivia.Games.GameUserLoan do
  use Ecto.Schema
  import Ecto.Changeset

  schema "game_user_loans" do
    field :amount, :integer, null: false
    field :amount_owing, :integer, null: false
    field :interest_rate, :float, null: false
    field :interest_rate_string, :string, null: false

    belongs_to(:game_user, Trivia.Games.GameUser)

    timestamps()
  end

  @doc false
  def changeset(game_user_loan, attrs) do
    game_user_loan
    |> cast(attrs, [:amount, :amount_owing, :interest_rate, :interest_rate_string, :game_user_id])
    |> validate_required([:amount, :amount_owing, :interest_rate, :interest_rate_string])
    |> validate_number(:amount, greater_than: 0)
    |> validate_number(:amount_owing, greater_than: 0)
  end
end
