defmodule Trivia.Games.GameQuestion do
  use Ecto.Schema
  import Ecto.Changeset

  schema "game_questions" do
    field :answers_collected_at, :utc_datetime
    field :asked_at, :utc_datetime
    field :bidding_ended_at, :utc_datetime

    belongs_to(:game, Trivia.Games.Game)
    belongs_to(:question, Trivia.Questions.Question)

    has_many(:game_question_users, Trivia.Games.GameQuestionUser, on_delete: :delete_all)

    has_many(:game_question_answers, Trivia.Games.GameQuestionAnswer, on_delete: :delete_all)

    timestamps()
  end

  @doc false
  def changeset(game_question, attrs) do
    game_question
    |> cast(attrs, [:asked_at, :answers_collected_at, :bidding_ended_at, :game_id, :question_id])
    |> validate_required([:game_id, :question_id])
  end
end
