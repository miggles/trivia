defmodule Trivia.Games.Summaries do
  alias Trivia.{
    Games,
    Repo
  }

  import Ecto.Query

  defmodule GameResult do
    defstruct game_user_id: nil,
              user_id: nil,
              username: nil,
              cash: nil,
              rank: nil,
              loans: nil,
              score: nil
  end

  defmodule GameQuestionUser do
    defstruct game_user_id: nil, user_id: nil, username: nil, delay: nil
  end

  defmodule GameQuestionResult do
    defstruct game_user_id: nil,
              user_id: nil,
              username: nil,
              bids: nil,
              payout: nil,
              winning_answer_payout: nil,
              correct_answer_payout: nil,
              total_payout: nil
  end

  defmodule GameQuestionAnswer do
    defstruct game_question_answer_id: nil,
              answer: nil,
              payout_string: nil,
              payout_rate: nil,
              users: nil,
              bids: nil,
              bid: nil # Shouldn't be here
  end

  defmodule GameQuestionAnswerBid do
    defstruct game_user_id: nil,
              username: nil,
              user_id: nil,
              payout: nil,
              bid: nil
  end

  def game_results(game_id) do
    # Get all users in the game, sorted by how much cash they ended with.
    Repo.all(
      from gu in Games.GameUser,
        join: users in assoc(gu, :user),
        where: gu.game_id == ^game_id,
        order_by: [desc: gu.cash],
        preload: [:user]
    )
    |> Enum.map(fn game_user ->
      %GameResult{
        game_user_id: game_user.id,
        user_id: game_user.user_id,
        username: game_user.user.username,
        cash: game_user.cash,
        loans: game_user.loans_owing,
        score: game_user.cash - game_user.loans_owing
      }
    end)
    |> Enum.sort_by(&{&1.score})
    |> Enum.reverse()
    |> Enum.with_index(1)
    |> Enum.map(fn {result, rank} -> %GameResult{result | rank: rank} end)
  end

  def game_question_users(game_question_id) do
    game_question =
      Repo.one(
        from gq in Games.GameQuestion,
          where: gq.id == ^game_question_id,
          preload: [game_question_users: [game_user: :user]]
      )

    game_question.game_question_users
    |> Enum.map(fn game_question_user ->
      %GameQuestionUser{
        user_id: game_question_user.game_user.user_id,
        game_user_id: game_question_user.game_user.id,
        username: game_question_user.game_user.user.username,
        delay: game_question_user.delay
      }
    end)
    |> Enum.sort_by(& &1.delay)
  end

  def game_question_results(game_id, game_question_id) do
    # Get a list of answers for this question. Should be able to use this as a
    # subquery in ecto but I can't figure out how to make it work.
    query =
      from gqa in Games.GameQuestionAnswer,
        select: gqa.id,
        where: gqa.game_question_id == ^game_question_id

    game_question_answer_ids = Repo.all(query)

    query =
      from gu in Games.GameUser,
        join: users in assoc(gu, :user),
        left_join: bids in assoc(gu, :game_question_answer_game_user_bids),
        on: bids.game_question_answer_id in ^game_question_answer_ids,
        left_join: answer in assoc(bids, :game_question_answer),
        on: answer.game_question_id == ^game_question_id,
        left_join: gqagu in assoc(answer, :game_question_answer_game_users),
        on: gqagu.game_user_id == gu.id,
        where: gu.game_id == ^game_id,
        group_by: [gu.id, users.id],
        select: [
          users.id,
          users.username,
          gu.id,
          sum(bids.payout),
          sum(bids.bid),
          sum(gqagu.winning_answer_payout),
          sum(gqagu.correct_answer_payout)
        ]

    query
    |> Repo.all()
    |> Enum.map(fn [
                     user_id,
                     username,
                     game_user_id,
                     payout,
                     bids,
                     winning_answer_payout,
                     correct_answer_payout
                   ] ->
      %GameQuestionResult{
        user_id: user_id,
        username: username,
        game_user_id: game_user_id,
        payout: payout || 0,
        bids: bids || 0,
        winning_answer_payout: winning_answer_payout || 0,
        correct_answer_payout: correct_answer_payout || 0,
        total_payout: (payout || 0) + (winning_answer_payout || 0) + (correct_answer_payout || 0)
      }
    end)
    |> Enum.sort(fn a, b -> a.total_payout > b.total_payout end)
  end

  def game_question_answers(game_question_id) do
    game_question_id
    |> Games.get_preloaded_game_question_answers()
    |> Enum.map(fn gqa ->
      users =
        gqa.game_question_answer_game_users
        |> Enum.map(& &1.game_user.user.username)

      %GameQuestionAnswer{
        game_question_answer_id: gqa.id,
        answer: gqa.answer,
        payout_string: gqa.payout_string,
        payout_rate: gqa.payout_rate,
        users: users
      }
    end)
    |> Enum.sort_by(& &1.answer)
    |> Enum.reverse()
  end

  def game_question_answer_bids(game_question_id) do
    game_question_id
    |> Games.get_preloaded_game_question_answers()
    |> Enum.map(fn gqa ->
      bids =
        gqa.game_question_answer_game_user_bids
        |> Enum.map(
          &%GameQuestionAnswerBid{
            bid: &1.bid,
            payout: &1.payout,
            game_user_id: &1.game_user.id,
            user_id: &1.game_user.user.id,
            username: &1.game_user.user.username
          }
        )

      {gqa.id, bids}
    end)
    |> Enum.into(%{})
  end
end
