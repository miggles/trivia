defmodule Trivia.Games.GameQuestionUser do
  use Ecto.Schema
  import Ecto.Changeset

  schema "game_question_users" do
    field :answer, :integer, null: false
    field :delay, :integer, null: false, default: 30_000

    belongs_to(:game_question, Trivia.Games.GameQuestion)
    belongs_to(:game_user, Trivia.Games.GameUser)

    timestamps()
  end

  @doc false
  def changeset(game_question_user, attrs) do
    game_question_user
    |> cast(attrs, [:answer, :game_question_id, :game_user_id, :delay])
    |> validate_required([:answer, :game_question_id, :game_user_id, :delay])
    |> validate_number(:answer, greater_than_or_equal_to: 0)
    |> validate_number(:delay, greater_than_or_equal_to: 0.0)
    |> unsafe_validate_unique([:game_question_id, :game_user_id], Trivia.Repo)
  end
end
