defmodule Trivia.Games.GameQuestionAnswer do
  use Ecto.Schema
  import Ecto.Changeset

  schema "game_question_answers" do
    field :answer, :integer, null: false
    field :correct, :boolean, default: false
    field :winner, :boolean, default: false
    field :payout_string, :string, null: false
    field :payout_rate, :float, null: false

    belongs_to(:game_question, Trivia.Games.GameQuestion)

    has_many(:game_question_answer_game_users, Trivia.Games.GameQuestionAnswerGameUser,
      on_delete: :delete_all
    )

    has_many(:game_question_answer_game_user_bids, Trivia.Games.GameQuestionAnswerGameUserBid,
      on_delete: :delete_all
    )

    timestamps()
  end

  @doc false
  def changeset(game_question_answer, attrs) do
    game_question_answer
    |> cast(attrs, [:answer, :winner, :correct, :payout_rate, :payout_string, :game_question_id])
    |> cast_assoc(:game_question_answer_game_users)
    |> validate_required([
      :answer,
      :winner,
      :correct,
      :payout_rate,
      :payout_string,
      :game_question_id
    ])
    |> validate_number(:answer,
      greater_than_or_equal_to: Application.get_env(:trivia, :minimum_value),
      less_than_or_equal_to: Application.get_env(:trivia, :max_value)
    )
    |> validate_number(:payout_rate, greater_than: 0)
  end
end
