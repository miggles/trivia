defmodule Trivia.Loanshark do
  @interest_rates [
    {1.2, "1.2x"},
    {1.5, "1.5x"},
    {2.0, "2.0x"},
    {2.5, "2.5x"},
    {3.0, "3.0x"}
  ]

  import Ecto.Query

  alias Trivia.{
    Games,
    Games.Game,
    Games.GameUserLoan,
    Repo
  }

  defmodule Loan do
    defstruct game_user_id: nil,
              amount: nil,
              amount_owing: nil,
              interest_rate: nil,
              interest_rate_string: nil
  end

  def issue_loan(game_user_id, %Loan{} = loan) do
    Repo.transaction(fn ->
      # Create the loan
      {:ok, _} = Games.create_game_user_loan(Map.from_struct(loan))

      # Update the game user
      Ecto.Adapters.SQL.query!(
        Repo,
        "UPDATE game_users SET cash = cash + $1, loans_owing = loans_owing + $2 WHERE id = $3",
        [loan.amount, loan.amount_owing, loan.game_user_id]
      )
    end)

    game_user = Games.get_game_user!(game_user_id)

    # Alert all subscribers that the game user has been updated
    Games.notify_subscribers(game_user, [:game_user, :updated])

    {:ok, game_user, get_next_game_user_loan(game_user)}
  end

  def get_next_game_user_loan(game_user) do
    {interest_rate, interest_rate_string} = get_interest_rate(get_loan_count(game_user))

    # What's the starting money for this game?
    amount = Repo.one(from g in Game, where: g.id == ^game_user.game_id, select: g.starting_cash)

    %Loan{
      game_user_id: game_user.id,
      amount: amount,
      amount_owing: round(amount * interest_rate),
      interest_rate: interest_rate,
      interest_rate_string: interest_rate_string
    }
  end

  defp get_interest_rate(loan_number) do
    Enum.at(@interest_rates, loan_number, List.last(@interest_rates))
  end

  defp get_loan_count(game_user) do
    game_user_id = game_user.id

    Repo.one(
      from gul in GameUserLoan, select: count(gul.id), where: gul.game_user_id == ^game_user.id
    )
  end
end
