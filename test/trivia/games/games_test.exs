defmodule Trivia.GamesTest do
  use Trivia.DataCase

  alias Trivia.Games

  describe "games" do
    alias Trivia.Games.Game

    test "list_games/0 returns all games" do
      game = insert(:game)
      assert Games.list_games() == [game]
    end

    test "get_game!/1 returns the game with given id" do
      game = insert(:game)
      assert Games.get_game!(game.id).id == game.id
    end

    test "create_game/1 with valid data creates a game" do
      user = insert(:user)
      attrs = params_for(:game, user: user)
      assert {:ok, %Game{} = game} = Games.create_game(attrs)
    end

    test "create_game/1 with valid data assigns questions to the game" do
      for _ <- 0..3, do: insert(:question)

      user = insert(:user)
      attrs = params_for(:game, user: user, number_of_questions: 2)
      assert {:ok, %Game{} = game} = Games.create_game(attrs)
      assert Enum.count(game.questions) == 2
      assert Enum.count(Enum.uniq(Enum.map(game.questions, & &1.id))) == 2
    end

    test "create_game/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Games.create_game(%{})
    end

    test "update_game/2 with valid data updates the game" do
      game = insert(:game)
      assert {:ok, %Game{} = game} = Games.update_game(game, %{slug: "hello"})
    end

    test "update_game/2 with invalid data returns error changeset" do
      game = insert(:game)
      assert {:error, %Ecto.Changeset{}} = Games.update_game(game, %{starting_cash: 1})
      assert game.starting_cash == Games.get_game!(game.id).starting_cash
    end

    test "delete_game/1 deletes the game" do
      game = insert(:game)
      assert {:ok, %Game{}} = Games.delete_game(game)
      assert_raise Ecto.NoResultsError, fn -> Games.get_game!(game.id) end
    end

    test "change_game/1 returns a game changeset" do
      game = insert(:game)
      assert %Ecto.Changeset{} = Games.change_game(game)
    end
  end

  describe "game_users" do
    alias Trivia.Games.GameUser

    def game_user_fixture(attrs \\ %{}) do
      insert(:game_user, game_user_params(attrs))
    end

    def game_user_params(attrs \\ %{}) do
      params_for(:game_user, attrs)
    end

    test "list_game_users/0 returns all game_users" do
      game_user = Repo.one(GameUser, id: game_user_fixture().id)
      assert Games.list_game_users() == [game_user]
    end

    test "get_game_user!/1 returns the game_user with given id" do
      game_user = Repo.one(GameUser, id: game_user_fixture().id)
      assert Games.get_game_user!(game_user.id) == game_user
    end

    test "create_game_user/1 with valid data creates a game_user" do
      game = insert(:game)
      user = insert(:user)
      attrs = game_user_params(game: game, user: user, cash: 199)
      assert {:ok, %GameUser{} = game_user} = Games.create_game_user(attrs)
      assert game_user.cash == 199
    end

    test "create_game_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Games.create_game_user(%{})
    end

    test "update_game_user/2 with valid data updates the game_user" do
      game_user = game_user_fixture()

      assert {:ok, %GameUser{} = game_user} =
               Games.update_game_user(game_user, %{cash: game_user.cash + 100})
    end

    test "update_game_user/2 with invalid data returns error changeset" do
      game_user = game_user_fixture()
      assert {:error, %Ecto.Changeset{}} = Games.update_game_user(game_user, %{cash: -1})
      assert game_user.cash == Games.get_game_user!(game_user.id).cash
    end

    test "delete_game_user/1 deletes the game_user" do
      game_user = game_user_fixture()
      assert {:ok, %GameUser{}} = Games.delete_game_user(game_user)
      assert_raise Ecto.NoResultsError, fn -> Games.get_game_user!(game_user.id) end
    end

    test "change_game_user/1 returns a game_user changeset" do
      game_user = game_user_fixture()
      assert %Ecto.Changeset{} = Games.change_game_user(game_user)
    end
  end

  describe "game_questions" do
    alias Trivia.Games.GameQuestion

    def game_question_params(attrs \\ %{}) do
      params_for(:game_question, attrs)
    end

    def game_question_fixture(attrs \\ %{}) do
      insert(:game_question, attrs)
    end

    test "list_game_questions/0 returns all game_questions" do
      game_question = Repo.one(GameQuestion, id: game_question_fixture().id)
      assert Games.list_game_questions() == [game_question]
    end

    test "get_game_question!/1 returns the game_question with given id" do
      game_question = Repo.one(GameQuestion, id: game_question_fixture().id)
      assert Games.get_game_question!(game_question.id) == game_question
    end

    test "create_game_question/1 with valid data creates a game_question" do
      question = insert(:question)
      game = insert(:game)
      attrs = game_question_params(question: question, game: game)
      assert {:ok, %GameQuestion{} = game_question} = Games.create_game_question(attrs)
    end

    test "create_game_question/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Games.create_game_question(%{})
    end

    test "update_game_question/2 with valid data updates the game_question" do
      game_question = game_question_fixture()

      assert {:ok, %GameQuestion{} = game_question} =
               Games.update_game_question(game_question, %{
                 answers_collected_at: DateTime.from_naive!(~N[2011-05-18T15:01:01Z], "Etc/UTC")
               })

      assert game_question.answers_collected_at ==
               DateTime.from_naive!(~N[2011-05-18T15:01:01Z], "Etc/UTC")
    end

    test "update_game_question/2 with invalid data returns error changeset" do
      game_question = game_question_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Games.update_game_question(game_question, %{question_id: nil})

      assert game_question.question_id == Games.get_game_question!(game_question.id).question_id
    end

    test "delete_game_question/1 deletes the game_question" do
      game_question = game_question_fixture()
      assert {:ok, %GameQuestion{}} = Games.delete_game_question(game_question)
      assert_raise Ecto.NoResultsError, fn -> Games.get_game_question!(game_question.id) end
    end

    test "change_game_question/1 returns a game_question changeset" do
      game_question = game_question_fixture()
      assert %Ecto.Changeset{} = Games.change_game_question(game_question)
    end
  end

  describe "game_question_users" do
    alias Trivia.Games.GameQuestionUser

    def game_question_user_params(attrs \\ %{}) do
      params_for(:game_question_user, attrs)
    end

    def game_question_user_fixture(attrs \\ %{}) do
      insert(:game_question_user, game_question_user_params(attrs))
    end

    test "list_game_question_users/0 returns all game_question_users" do
      game_question_user = Repo.one(GameQuestionUser, id: game_question_user_fixture().id)
      assert Games.list_game_question_users() == [game_question_user]
    end

    test "get_game_question_user!/1 returns the game_question_user with given id" do
      game_question_user = Repo.one(GameQuestionUser, id: game_question_user_fixture().id)
      assert Games.get_game_question_user!(game_question_user.id) == game_question_user
    end

    test "create_game_question_user/1 with valid data creates a game_question_user" do
      game_question = insert(:game_question)
      game_user = insert(:game_user, game: game_question.game)

      attrs =
        game_question_user_params(game_user: game_user, game_question: game_question, answer: 42)

      assert {:ok, %GameQuestionUser{} = game_question_user} =
               Games.create_game_question_user(attrs)

      assert game_question_user.answer == 42
    end

    test "create_game_question_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Games.create_game_question_user(%{})
    end

    test "update_game_question_user/2 with valid data updates the game_question_user" do
      game_question_user = game_question_user_fixture()

      assert {:ok, %GameQuestionUser{} = game_question_user} =
               Games.update_game_question_user(game_question_user, %{answer: 150})

      assert game_question_user.answer == 150
    end

    test "update_game_question_user/2 with invalid data returns error changeset" do
      game_question_user = game_question_user_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Games.update_game_question_user(game_question_user, %{answer: nil})

      assert game_question_user.answer ==
               Games.get_game_question_user!(game_question_user.id).answer
    end

    test "delete_game_question_user/1 deletes the game_question_user" do
      game_question_user = game_question_user_fixture()
      assert {:ok, %GameQuestionUser{}} = Games.delete_game_question_user(game_question_user)

      assert_raise Ecto.NoResultsError, fn ->
        Games.get_game_question_user!(game_question_user.id)
      end
    end

    test "change_game_question_user/1 returns a game_question_user changeset" do
      game_question_user = game_question_user_fixture()
      assert %Ecto.Changeset{} = Games.change_game_question_user(game_question_user)
    end
  end

  describe "game_question_answers" do
    alias Trivia.Games.GameQuestionAnswer

    def game_question_answer_fixture(attrs \\ %{}) do
      insert(:game_question_answer, attrs)
    end

    def game_question_answer_params(attrs \\ %{}) do
      params_for(:game_question_answer, attrs)
    end

    test "list_game_question_answers/0 returns all game_question_answers" do
      game_question_answer = Repo.one(GameQuestionAnswer, id: game_question_answer_fixture().id)
      assert Games.list_game_question_answers() == [game_question_answer]
    end

    test "get_game_question_answer!/1 returns the game_question_answer with given id" do
      game_question_answer = Repo.one(GameQuestionAnswer, id: game_question_answer_fixture().id)
      assert Games.get_game_question_answer!(game_question_answer.id) == game_question_answer
    end

    test "create_game_question_answer/1 with valid data creates a game_question_answer" do
      game_question = insert(:game_question)

      attrs = game_question_answer_params(game_question: game_question)

      assert {:ok, %GameQuestionAnswer{} = game_question_answer} =
               Games.create_game_question_answer(attrs)

      assert game_question_answer.answer == 99
      assert game_question_answer.correct == false
      assert game_question_answer.winner == true
    end

    test "create_game_question_answer/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Games.create_game_question_answer(%{})
    end

    test "update_game_question_answer/2 with valid data updates the game_question_answer" do
      game_question_answer = game_question_answer_fixture()

      assert {:ok, %GameQuestionAnswer{} = game_question_answer} =
               Games.update_game_question_answer(game_question_answer, %{
                 answer: 43,
                 winner: false,
                 correct: false
               })

      assert game_question_answer.answer == 43
      assert game_question_answer.correct == false
      assert game_question_answer.winner == false
    end

    test "update_game_question_answer/2 with invalid data returns error changeset" do
      game_question_answer = game_question_answer_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Games.update_game_question_answer(game_question_answer, %{answer: nil})

      assert game_question_answer.answer ==
               Games.get_game_question_answer!(game_question_answer.id).answer
    end

    test "delete_game_question_answer/1 deletes the game_question_answer" do
      game_question_answer = game_question_answer_fixture()

      assert {:ok, %GameQuestionAnswer{}} =
               Games.delete_game_question_answer(game_question_answer)

      assert_raise Ecto.NoResultsError, fn ->
        Games.get_game_question_answer!(game_question_answer.id)
      end
    end

    test "change_game_question_answer/1 returns a game_question_answer changeset" do
      game_question_answer = game_question_answer_fixture()
      assert %Ecto.Changeset{} = Games.change_game_question_answer(game_question_answer)
    end
  end

  describe "game_question_answer_game_users" do
    alias Trivia.Games.GameQuestionAnswerGameUser

    def game_question_answer_game_user_fixture(attrs \\ %{}) do
      insert(:game_question_answer_game_user, attrs)
    end

    def game_question_answer_game_user_params(attrs \\ %{}) do
      params_for(:game_question_answer_game_user, attrs)
    end

    test "list_game_question_answer_game_users/0 returns all game_question_answer_game_users" do
      game_question_answer_game_user =
        Repo.one(GameQuestionAnswerGameUser, id: game_question_answer_game_user_fixture().id)

      assert Games.list_game_question_answer_game_users() == [game_question_answer_game_user]
    end

    test "get_game_question_answer_game_user!/1 returns the game_question_answer_game_user with given id" do
      game_question_answer_game_user =
        Repo.one(GameQuestionAnswerGameUser, id: game_question_answer_game_user_fixture().id)

      assert Games.get_game_question_answer_game_user!(game_question_answer_game_user.id) ==
               game_question_answer_game_user
    end

    test "create_game_question_answer_game_user/1 with valid data creates a game_question_answer_game_user" do
      game_question_answer = insert(:game_question_answer)
      game_user = insert(:game_user, game: game_question_answer.game_question.game)

      attrs =
        game_question_answer_game_user_params(
          game_question_answer: game_question_answer,
          game_user: game_user
        )

      assert {:ok, %GameQuestionAnswerGameUser{} = game_question_answer_game_user} =
               Games.create_game_question_answer_game_user(attrs)
    end

    test "create_game_question_answer_game_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} =
               Games.create_game_question_answer_game_user(%{game_user_id: nil})
    end

    # test "update_game_question_answer_game_user/2 with valid data updates the game_question_answer_game_user" do
    #   game_question_answer_game_user = game_question_answer_game_user_fixture()
    #   assert {:ok, %GameQuestionAnswerGameUser{} = game_question_answer_game_user} = Games.update_game_question_answer_game_user(game_question_answer_game_user, @update_attrs)
    # end

    test "update_game_question_answer_game_user/2 with invalid data returns error changeset" do
      game_question_answer_game_user = game_question_answer_game_user_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Games.update_game_question_answer_game_user(game_question_answer_game_user, %{
                 game_user_id: nil
               })

      assert game_question_answer_game_user.game_user_id ==
               Games.get_game_question_answer_game_user!(game_question_answer_game_user.id).game_user_id
    end

    test "delete_game_question_answer_game_user/1 deletes the game_question_answer_game_user" do
      game_question_answer_game_user = game_question_answer_game_user_fixture()

      assert {:ok, %GameQuestionAnswerGameUser{}} =
               Games.delete_game_question_answer_game_user(game_question_answer_game_user)

      assert_raise Ecto.NoResultsError, fn ->
        Games.get_game_question_answer_game_user!(game_question_answer_game_user.id)
      end
    end

    test "change_game_question_answer_game_user/1 returns a game_question_answer_game_user changeset" do
      game_question_answer_game_user = game_question_answer_game_user_fixture()

      assert %Ecto.Changeset{} =
               Games.change_game_question_answer_game_user(game_question_answer_game_user)
    end
  end

  describe "game_question_answer_game_user_bids" do
    alias Trivia.Games.GameQuestionAnswerGameUserBid

    def game_question_answer_game_user_bid_fixture(attrs \\ %{}) do
      insert(:game_question_answer_game_user_bid, attrs)
    end

    def game_question_answer_game_user_bid_params(attrs \\ %{}) do
      params_for(:game_question_answer_game_user_bid, attrs)
    end

    test "list_game_question_answer_game_user_bids/0 returns all game_question_answer_game_user_bids" do
      game_question_answer_game_user_bid =
        Repo.one(GameQuestionAnswerGameUserBid,
          id: game_question_answer_game_user_bid_fixture().id
        )

      assert Games.list_game_question_answer_game_user_bids() == [
               game_question_answer_game_user_bid
             ]
    end

    test "get_game_question_answer_game_user_bid!/1 returns the game_question_answer_game_user_bid with given id" do
      game_question_answer_game_user_bid =
        Repo.one(GameQuestionAnswerGameUserBid,
          id: game_question_answer_game_user_bid_fixture().id
        )

      assert Games.get_game_question_answer_game_user_bid!(game_question_answer_game_user_bid.id) ==
               game_question_answer_game_user_bid
    end

    test "create_game_question_answer_game_user_bid/1 with valid data creates a game_question_answer_game_user_bid" do
      game_question_answer = insert(:game_question_answer)
      game_user = insert(:game_user, game: game_question_answer.game_question.game)

      attrs =
        game_question_answer_game_user_bid_params(
          game_question_answer: game_question_answer,
          game_user: game_user
        )

      assert {:ok, %GameQuestionAnswerGameUserBid{} = game_question_answer_game_user_bid} =
               Games.create_game_question_answer_game_user_bid(attrs)

      assert game_question_answer_game_user_bid.bid == 200
      assert game_question_answer_game_user_bid.payout == 400
    end

    test "create_game_question_answer_game_user_bid/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Games.create_game_question_answer_game_user_bid(%{})
    end

    test "update_game_question_answer_game_user_bid/2 with valid data updates the game_question_answer_game_user_bid" do
      game_question_answer_game_user_bid = game_question_answer_game_user_bid_fixture()

      assert {:ok, %GameQuestionAnswerGameUserBid{} = game_question_answer_game_user_bid} =
               Games.update_game_question_answer_game_user_bid(
                 game_question_answer_game_user_bid,
                 %{bid: 500, payout: 1_000}
               )

      assert game_question_answer_game_user_bid.bid == 500
      assert game_question_answer_game_user_bid.payout == 1_000
    end

    test "update_game_question_answer_game_user_bid/2 with invalid data returns error changeset" do
      game_question_answer_game_user_bid = game_question_answer_game_user_bid_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Games.update_game_question_answer_game_user_bid(
                 game_question_answer_game_user_bid,
                 %{bid: nil}
               )

      assert game_question_answer_game_user_bid.bid ==
               Games.get_game_question_answer_game_user_bid!(
                 game_question_answer_game_user_bid.id
               ).bid
    end

    test "delete_game_question_answer_game_user_bid/1 deletes the game_question_answer_game_user_bid" do
      game_question_answer_game_user_bid = game_question_answer_game_user_bid_fixture()

      assert {:ok, %GameQuestionAnswerGameUserBid{}} =
               Games.delete_game_question_answer_game_user_bid(game_question_answer_game_user_bid)

      assert_raise Ecto.NoResultsError, fn ->
        Games.get_game_question_answer_game_user_bid!(game_question_answer_game_user_bid.id)
      end
    end

    test "change_game_question_answer_game_user_bid/1 returns a game_question_answer_game_user_bid changeset" do
      game_question_answer_game_user_bid = game_question_answer_game_user_bid_fixture()

      assert %Ecto.Changeset{} =
               Games.change_game_question_answer_game_user_bid(game_question_answer_game_user_bid)
    end
  end

  describe "game_user_loans" do
    alias Trivia.Games.GameUserLoan

    def game_user_loan_fixture(attrs \\ %{}) do
      insert(:game_user_loan, attrs)
    end

    def game_user_loan_params(attrs \\ %{}) do
      params_with_assocs(:game_user_loan, attrs)
    end

    test "list_game_user_loans/0 returns all game_user_loans" do
      game_user_loan = game_user_loan_fixture()
      assert Games.list_game_user_loans() == [Games.get_game_user_loan!(game_user_loan.id)]
    end

    test "get_game_user_loan!/1 returns the game_user_loan with given id" do
      game_user_loan = game_user_loan_fixture()
      assert Games.get_game_user_loan!(game_user_loan.id).id == game_user_loan.id
    end

    test "create_game_user_loan/1 with valid data creates a game_user_loan" do
      assert {:ok, %GameUserLoan{} = game_user_loan} =
               Games.create_game_user_loan(game_user_loan_params())

      assert game_user_loan.amount == 200
      assert game_user_loan.amount_owing == 400
      assert game_user_loan.interest_rate == 2.0
      assert game_user_loan.interest_rate_string == "2.0x"
    end

    test "create_game_user_loan/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Games.create_game_user_loan(%{})
    end

    test "update_game_user_loan/2 with valid data updates the game_user_loan" do
      game_user_loan = game_user_loan_fixture()

      assert {:ok, %GameUserLoan{} = game_user_loan} =
               Games.update_game_user_loan(game_user_loan, %{amount: 100, amount_owing: 200})

      assert game_user_loan.amount == 100
      assert game_user_loan.amount_owing == 200
    end

    test "update_game_user_loan/2 with invalid data returns error changeset" do
      game_user_loan = game_user_loan_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Games.update_game_user_loan(game_user_loan, %{amount: -1})

      assert game_user_loan.amount == Games.get_game_user_loan!(game_user_loan.id).amount
    end

    test "delete_game_user_loan/1 deletes the game_user_loan" do
      game_user_loan = game_user_loan_fixture()
      assert {:ok, %GameUserLoan{}} = Games.delete_game_user_loan(game_user_loan)
      assert_raise Ecto.NoResultsError, fn -> Games.get_game_user_loan!(game_user_loan.id) end
    end

    test "change_game_user_loan/1 returns a game_user_loan changeset" do
      game_user_loan = game_user_loan_fixture()
      assert %Ecto.Changeset{} = Games.change_game_user_loan(game_user_loan)
    end
  end
end
