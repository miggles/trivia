defmodule Trivia.Games.Process.CreateGameAnswersTest do
  use Trivia.DataCase

  alias Trivia.Games.Process.CreateGameAnswers

  @answer 100

  setup do
    question = insert(:question, answer: @answer)
    game_question = insert(:game_question, question: question)
    {:ok, %{game_question: game_question}}
  end

  describe "for/1 with zero user answers" do
    test "no answers are present", %{game_question: game_question} do
      {:ok, []} = CreateGameAnswers.for(game_question)

      game_question = Repo.preload(game_question, :game_question_answers)

      assert game_question.game_question_answers == []
    end
  end

  describe "for/1 with one user answer" do
    setup %{game_question: game_question} = opts do
      game_user = insert(:game_user, game: game_question.game)
      {:ok, Map.put(opts, :game_user, game_user)}
    end

    test "it has the median payout", %{game_question: game_question, game_user: game_user} do
      insert(:game_question_user, game_question: game_question, game_user: game_user)

      {:ok, [game_question_answer]} = CreateGameAnswers.for(game_question)

      assert game_question_answer.payout_rate == 2.0
      assert game_question_answer.payout_string == "2:1"
    end

    test "it has the user assigned", %{game_question: game_question, game_user: game_user} do
      insert(:game_question_user, game_question: game_question, game_user: game_user)

      {:ok, [game_question_answer]} = CreateGameAnswers.for(game_question)

      game_question_answer =
        Repo.preload(game_question_answer, game_question_answer_game_users: :game_user)

      game_users = Enum.map(game_question_answer.game_question_answer_game_users, & &1.game_user)

      game_user = Repo.one(Trivia.Games.GameUser, id: game_user.id)

      assert game_users == [game_user]
    end

    test "it is flagged as non-winning when over the answer", %{
      game_question: game_question,
      game_user: game_user
    } do
      insert(:game_question_user,
        game_question: game_question,
        game_user: game_user,
        answer: @answer + 1
      )

      {:ok, [game_question_answer]} = CreateGameAnswers.for(game_question)

      assert game_question_answer.correct == false
      assert game_question_answer.winner == false

      # We should not get any bonus
      gqagu = List.first(game_question_answer.game_question_answer_game_users)
      assert gqagu.correct_answer_payout == 0
      assert gqagu.winning_answer_payout == 0
    end

    test "it is flagged as winning and correct when it is the answer", %{
      game_question: game_question,
      game_user: game_user
    } do
      insert(:game_question_user,
        game_question: game_question,
        game_user: game_user,
        answer: @answer
      )

      {:ok, [game_question_answer]} = CreateGameAnswers.for(game_question)

      assert game_question_answer.correct == true
      assert game_question_answer.winner == true

      # Check that the user gets a correct bonus
      gqagu = List.first(game_question_answer.game_question_answer_game_users)
      assert gqagu.correct_answer_payout > 0
      assert gqagu.winning_answer_payout == 0
    end

    test "it is flagged as winning when under the answer", %{
      game_question: game_question,
      game_user: game_user
    } do
      insert(:game_question_user,
        game_question: game_question,
        game_user: game_user,
        answer: @answer - 1
      )

      {:ok, [game_question_answer]} = CreateGameAnswers.for(game_question)

      assert game_question_answer.correct == false
      assert game_question_answer.winner == true

      # Check that the user gets a winning bonus
      gqagu = List.first(game_question_answer.game_question_answer_game_users)
      assert gqagu.correct_answer_payout == 0
      assert gqagu.winning_answer_payout > 0
    end
  end

  describe "for/1 with duplicate answers" do
    test "when all are unique", %{game_question: game_question} do
      insert(:game_question_user, game_question: game_question, answer: @answer - 10)
      insert(:game_question_user, game_question: game_question, answer: @answer + 10)
      insert(:game_question_user, game_question: game_question, answer: @answer)

      {:ok, [gqa_1, gqa_2, gqa_3]} = CreateGameAnswers.for(game_question)

      assert gqa_1.answer == 110
      assert gqa_1.payout_rate == 2.5
      assert gqa_1.correct == false
      assert gqa_1.winner == false
      assert Enum.count(gqa_1.game_question_answer_game_users) == 1

      assert gqa_2.answer == 100
      assert gqa_2.payout_rate == 2.0
      assert gqa_2.correct == true
      assert gqa_2.winner == true
      assert Enum.count(gqa_2.game_question_answer_game_users) == 1

      assert gqa_3.answer == 90
      assert gqa_3.payout_rate == 2.5
      assert gqa_3.correct == false
      assert gqa_3.winner == false
      assert Enum.count(gqa_3.game_question_answer_game_users) == 1
    end

    test "with two unique values", %{game_question: game_question} do
      insert(:game_question_user, game_question: game_question, answer: @answer + 10)
      insert(:game_question_user, game_question: game_question, answer: @answer + 10)
      insert(:game_question_user, game_question: game_question, answer: @answer)

      {:ok, [gqa_1, gqa_2]} = CreateGameAnswers.for(game_question)

      assert gqa_1.answer == 110
      assert gqa_1.payout_rate == 2.5
      assert gqa_1.correct == false
      assert gqa_1.winner == false
      assert Enum.count(gqa_1.game_question_answer_game_users) == 2

      assert gqa_2.answer == 100
      assert gqa_2.payout_rate == 2.0
      assert gqa_2.correct == true
      assert gqa_2.winner == true
      assert Enum.count(gqa_2.game_question_answer_game_users) == 1
    end

    test "with one unique value", %{game_question: game_question} do
      insert(:game_question_user, game_question: game_question, answer: @answer)
      insert(:game_question_user, game_question: game_question, answer: @answer)
      insert(:game_question_user, game_question: game_question, answer: @answer)

      {:ok, [gqa]} = CreateGameAnswers.for(game_question)

      assert gqa.answer == 100
      assert gqa.payout_rate == 2.0
      assert gqa.correct == true
      assert gqa.winner == true
      assert Enum.count(gqa.game_question_answer_game_users) == 3
    end
  end

  describe "for/1 with many user answers" do
    test "the payout rates are distributed above and below", %{game_question: game_question} do
      for i <- 0..20 do
        insert(:game_question_user, game_question: game_question, answer: i)
      end

      {:ok, answers} = CreateGameAnswers.for(game_question)

      relevant = Enum.map(answers, &{&1.answer, &1.payout_rate, &1.winner, &1.correct})

      assert relevant ==
               [
                 # Outlier, winning answer
                 {20, 5.0, true, false},
                 {19, 5.0, false, false},
                 {18, 4.5, false, false},
                 {17, 4.5, false, false},
                 {16, 4.0, false, false},
                 {15, 3.5, false, false},
                 {14, 3.5, false, false},
                 {13, 3.0, false, false},
                 {12, 3.0, false, false},
                 {11, 2.5, false, false},
                 # Median
                 {10, 2.0, false, false},
                 {9, 2.5, false, false},
                 {8, 3.0, false, false},
                 {7, 3.0, false, false},
                 {6, 3.5, false, false},
                 {5, 3.5, false, false},
                 {4, 4.0, false, false},
                 {3, 4.5, false, false},
                 {2, 4.5, false, false},
                 {1, 5.0, false, false},
                 # Outlier
                 {0, 5.0, false, false}
               ]
    end
  end

  describe "for/1 with too many answers" do
    test "a range of answers are dropped to reduce the selection pool", %{game_question: game_question} do
      for i <- 0..31 do
        insert(:game_question_user, game_question: game_question, answer: i)
      end

      {:ok, answers} = CreateGameAnswers.for(game_question)

      assert Enum.count(answers) == Application.get_env(:trivia, :maximum_answers)
    end
  end
end
