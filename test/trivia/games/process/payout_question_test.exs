defmodule Trivia.Games.Process.PayoutQuestionTest do
  use Trivia.DataCase

  alias Trivia.Games
  alias Trivia.Games.Process.PayoutQuestion

  @answer 100

  setup do
    question = insert(:question, answer: @answer)
    game_question = insert(:game_question, question: question)
    {:ok, %{game_question: game_question}}
  end

  describe "finalise_payouts/1" do
    test "it pays everything out", %{game_question: game_question} do
      # The winning answer
      winning_game_question_answer =
        insert(:game_question_answer, game_question: game_question, winner: true)

      losing_game_question_answer =
        insert(:game_question_answer, game_question: game_question, winner: false)

      # 1. A user who got a winning_answer_payout as well as a bid payout
      game_user_1 = insert(:game_user, game: game_question.game, cash: 0)

      insert(:game_question_answer_game_user,
        game_user: game_user_1,
        game_question_answer: winning_game_question_answer,
        winning_answer_payout: 100
      )

      insert(:game_question_answer_game_user_bid,
        game_user: game_user_1,
        game_question_answer: winning_game_question_answer,
        payout: 50
      )

      # 2. A user who got a bid payout
      game_user_2 = insert(:game_user, game: game_question.game, cash: 0)

      insert(:game_question_answer_game_user_bid,
        game_user: game_user_2,
        game_question_answer: winning_game_question_answer,
        payout: 50
      )

      # 3. A user who did not get anything
      game_user_3 = insert(:game_user, game: game_question.game, cash: 0)

      insert(:game_question_answer_game_user_bid,
        game_user: game_user_3,
        game_question_answer: losing_game_question_answer,
        payout: 0
      )

      # 4. A user who did not bid.
      game_user_4 = insert(:game_user, game: game_question.game, cash: 0)

      # Run the payout
      PayoutQuestion.finalise_payouts(game_question.id)

      # Now let's check the final cash for each user
      assert Games.get_game_user!(game_user_1.id).cash == 150
      assert Games.get_game_user!(game_user_2.id).cash == 50
      assert Games.get_game_user!(game_user_3.id).cash == 0
      assert Games.get_game_user!(game_user_4.id).cash == 0

      # TODO: Set a paid flag on the game_question_answer_game_user?
      # TODO: Check the paid flag on the bids
    end
  end
end
