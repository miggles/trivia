defmodule Trivia.Games.SummariesTest do
  use Trivia.DataCase

  alias Trivia.Games.Summaries

  describe "game_results/1" do
    #
  end

  describe "game_question_users/1" do
    test "returns a list of users who have answered the question" do
      game_question = insert(:game_question)

      game_user_1 =
        insert(:game_question_user, game_question: game_question, delay: 3_210).game_user

      game_user_2 =
        insert(:game_question_user, game_question: game_question, delay: 2_109).game_user

      # Add a third game user who hasn't answered the question and won't show up
      insert(:game_user, game: game_question.game)

      game_user_1_summary = %Trivia.Games.Summaries.GameQuestionUser{
        user_id: game_user_1.user_id,
        game_user_id: game_user_1.id,
        username: game_user_1.user.username,
        delay: 3_210
      }

      game_user_2_summary = %Trivia.Games.Summaries.GameQuestionUser{
        user_id: game_user_2.user_id,
        game_user_id: game_user_2.id,
        username: game_user_2.user.username,
        delay: 2_109
      }

      assert [^game_user_2_summary, ^game_user_1_summary] =
               Summaries.game_question_users(game_question.id)
    end
  end

  describe "game_question_answers/2" do
    #
  end

  describe "game_question_results/2" do
    test "returns an accurate map of results" do
      game = insert(:game)

      game_question = insert(:game_question, game: game)

      question = game_question.question

      winning_game_question_answer =
        insert(:game_question_answer,
          winner: true,
          correct: true,
          answer: question.answer,
          game_question: game_question
        )

      losing_game_question_answer_1 =
        insert(:game_question_answer,
          winner: false,
          correct: false,
          answer: question.answer - 10,
          game_question: game_question
        )

      losing_game_question_answer_2 =
        insert(:game_question_answer,
          winner: false,
          correct: false,
          answer: question.answer + 10,
          game_question: game_question
        )

      # 1. A user with one winning bid who also happened to supply the right answer
      game_user_1 = insert(:game_user, game: game, cash: 1020)

      insert(:game_question_answer_game_user,
        game_user: game_user_1,
        game_question_answer: winning_game_question_answer,
        winning_answer_payout: 150
      )

      insert(:game_question_answer_game_user_bid,
        game_question_answer: winning_game_question_answer,
        game_user: game_user_1,
        bid: 10,
        payout: 20
      )

      # 2. A user with one winning bid and one losing bid
      game_user_2 = insert(:game_user, game: game, cash: 920)

      insert(:game_question_answer_game_user_bid,
        game_question_answer: winning_game_question_answer,
        game_user: game_user_2,
        bid: 100,
        payout: 200
      )

      insert(:game_question_answer_game_user_bid,
        game_question_answer: losing_game_question_answer_1,
        game_user: game_user_2,
        bid: 10,
        payout: 0
      )

      # 3. A user with one losing bid
      game_user_3 = insert(:game_user, game: game, cash: 50)

      insert(:game_question_answer_game_user_bid,
        game_question_answer: losing_game_question_answer_1,
        game_user: game_user_3,
        bid: 10,
        payout: 0
      )

      # 4. A user with two losing bids
      game_user_4 = insert(:game_user, game: game, cash: 500)

      insert(:game_question_answer_game_user_bid,
        game_question_answer: losing_game_question_answer_1,
        game_user: game_user_4,
        bid: 10,
        payout: 0
      )

      insert(:game_question_answer_game_user_bid,
        game_question_answer: losing_game_question_answer_2,
        game_user: game_user_4,
        bid: 10,
        payout: 0
      )

      # 5. A user with no bids
      game_user_5 = insert(:game_user, game: game, cash: 10_000)
      # No answers.

      # Now get the results
      results = Summaries.game_question_results(game.id, game_question.id)

      # The order of the game users should be by payout and then by cash
      assert Enum.map(results, & &1.payout) == [200, 20, 0, 0, 0]

      assert Enum.map(results, & &1.bids) == [110, 10, 0, 20, 10]

      assert Enum.map(results, & &1.game_user_id) == [
               game_user_2.id,
               game_user_1.id,
               game_user_5.id,
               game_user_4.id,
               game_user_3.id
             ]

      assert Enum.map(results, & &1.winning_answer_payout) == [0, 150, 0, 0, 0]

      assert Enum.map(results, & &1.correct_answer_payout) == [0, 0, 0, 0, 0]

      assert Enum.map(results, & &1.total_payout) == [200, 170, 0, 0, 0]
    end

    test "does not include unrelated data" do
      game = insert(:game)
      game_user = insert(:game_user, game: game)

      # User bids on the first question and wins, does not bid on the second.
      game_question_1 = insert(:game_question, game: game)
      game_question_answer_1 = insert(:game_question_answer, game_question: game_question_1)

      insert(:game_question_answer_game_user_bid,
        game_question_answer: game_question_answer_1,
        game_user: game_user,
        bid: 10,
        payout: 20
      )

      game_question_2 = insert(:game_question, game: game)
      insert(:game_question_answer, game_question: game_question_2)

      # Generate the summary for the second question
      [result] = Summaries.game_question_results(game.id, game_question_2.id)

      assert result.payout == 0
    end
  end
end
