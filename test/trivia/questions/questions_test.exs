defmodule Trivia.QuestionsTest do
  use Trivia.DataCase

  alias Trivia.Questions

  describe "questions" do
    alias Trivia.Questions.Question

    test "list_questions/0 returns all questions" do
      question = insert(:question)
      assert Questions.list_questions() == [question |> Repo.preload(:user)]
    end

    test "get_question!/1 returns the question with given id" do
      question = insert(:question)
      assert Questions.get_question!(question.id).id == question.id
    end

    test "create_question/1 with valid data creates a question" do
      user = insert(:user)
      attrs = params_for(:question, user: user)

      assert {:ok, %Question{} = question} = Questions.create_question(attrs)
      assert question.answer == 100
      assert question.question == "Question?"
      assert question.question_html == "<p>Question?</p>\n"
      assert question.citation == "~~~baleeted~~~"
      assert question.citation_html == "<p><del>~baleeted</del>~</p>\n"
      assert question.unit == "m"

      question = Repo.preload(question, :user)
      assert question.user == user
    end

    test "create_question/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Questions.create_question(%{})
    end

    test "update_question/2 with valid data updates the question" do
      question = insert(:question)
      assert {:ok, %Question{} = question} = Questions.update_question(question, %{answer: 200})
      assert question.answer == 200
    end

    test "update_question/2 with invalid data returns error changeset" do
      question = insert(:question)

      assert {:error, %Ecto.Changeset{}} =
               Questions.update_question(question, %{answer: "Invalid"})

      assert question.answer == Questions.get_question!(question.id).answer
    end

    test "delete_question/1 deletes the question" do
      question = insert(:question)
      assert {:ok, %Question{}} = Questions.delete_question(question)
      assert_raise Ecto.NoResultsError, fn -> Questions.get_question!(question.id) end
    end

    test "change_question/1 returns a question changeset" do
      question = insert(:question)
      assert %Ecto.Changeset{} = Questions.change_question(question)
    end
  end
end
