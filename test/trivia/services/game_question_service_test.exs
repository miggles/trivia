defmodule Trivia.GameQuestionServiceTest do
  use Trivia.DataCase

  alias Trivia.{
    Games,
    GameQuestionService
  }

  describe "answering" do
    setup do
      game_question = insert(:game_question)
      game = game_question.game
      game_user = insert(:game_user, game: game)

      # Start the game server supervised so it gets cleaned up automatically
      start_supervised!({Trivia.GameService, game.id})

      # Set the game state to asking
      {:ok, game} = Games.update_game(game, %{state: "asking_question"})

      {:ok,
       %{
         game: game,
         user: game_user.user,
         game_user: game_user,
         game_question: game_question
       }}
    end

    test "it does not allow answering with an incorrect state", opts do
      # Set the game state to bidding
      {:ok, _} = Games.update_game(opts.game, %{state: "bidding_on_question"})

      assert {:error, :incorrect_state} =
               GameQuestionService.answer(opts.game_question.id, opts.game_user.id, 1, 1_000)
    end

    test "it does not allow multiple answers from one user", opts do
      GameQuestionService.answer(opts.game_question.id, opts.game_user.id, 1, 1_000)

      assert {:error, changeset} =
               GameQuestionService.answer(opts.game_question.id, opts.game_user.id, 2, 1_000)

      assert changeset.valid? == false

      [{:game_question_id, {"has already been taken", _}}] = changeset.errors
    end

    test "it saves an answer", opts do
      assert {:ok, game_question_user} =
               GameQuestionService.answer(opts.game_question.id, opts.game_user.id, 1, 1_000)

      assert game_question_user.answer == 1
    end
  end

  describe "bidding" do
    @starting_cash 100
    @bid 25

    setup do
      game_question_answer = insert(:game_question_answer)
      game = game_question_answer.game_question.game
      game_user = insert(:game_user, game: game, cash: @starting_cash)

      # Set the game state to bidding
      {:ok, game} = Games.update_game(game, %{state: "bidding_on_question"})

      # Start the game server supervised so it gets cleaned up automatically
      start_supervised!({Trivia.GameService, game.id})

      {:ok,
       %{
         game: game,
         user: game_user.user,
         game_user: game_user,
         game_question: game_question_answer.game_question,
         game_question_answer: game_question_answer
       }}
    end

    test "it allows a user to bid on an answer", opts do
      assert {:ok, {game_user, bid}} =
               GameQuestionService.bid(
                 # Service id
                 opts.game_question.id,

                 # Operation params
                 opts.game_question_answer.id,
                 opts.game_user.id,
                 @bid
               )

      assert game_user.cash == @starting_cash - @bid

      assert bid.game_question_answer_id == opts.game_question_answer.id
      assert bid.game_user_id == game_user.id
      assert bid.bid == @bid
    end

    test "it stops the bid if the user doesn't have enough cash", opts do
      assert {:error, :insufficient_funds} =
               GameQuestionService.bid(
                 # Service id
                 opts.game_question.id,

                 # Operation params
                 opts.game_question_answer.id,
                 opts.game_user.id,
                 @starting_cash + @bid
               )

      # Reload the user
      game_user = Games.get_game_user!(opts.game_user.id)
      assert game_user.cash == @starting_cash

      # Ensure the bid exists
      game_question_answer =
        Games.get_game_question_answer!(opts.game_question_answer.id)
        |> Repo.preload(:game_question_answer_game_user_bids)

      [] = game_question_answer.game_question_answer_game_user_bids
    end

    test "it stops the bid if the state is displaying_question_results", opts do
      # We're done with this question
      {:ok, _} = Games.update_game(opts.game, %{state: "displaying_question_results"})

      assert {:error, :incorrect_state} =
               GameQuestionService.bid(
                 # Service id
                 opts.game_question.id,

                 # Operation params
                 opts.game_question_answer.id,
                 opts.game_user.id,
                 @bid
               )

      # Reload the user
      game_user = Games.get_game_user!(opts.game_user.id)
      assert game_user.cash == @starting_cash

      # Ensure the bid exists
      game_question_answer =
        Games.get_game_question_answer!(opts.game_question_answer.id)
        |> Repo.preload(:game_question_answer_game_user_bids)

      [] = game_question_answer.game_question_answer_game_user_bids
    end
  end
end
