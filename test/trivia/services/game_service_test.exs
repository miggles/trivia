defmodule Trivia.GameServiceTest do
  use Trivia.DataCase

  alias Trivia.{
    Games,
    GameService
  }

  describe "recovery" do
    test "it recovers to the correct question" do
      questions = [insert(:question), insert(:question), insert(:question)]

      game =
        insert(:game, questions: questions, number_of_questions: 3)
        |> Repo.preload(:game_questions)

      # Now update the game so that it's asking the second question
      [_, game_question, _] = game.game_questions
      {:ok, now} = DateTime.now("Etc/UTC")

      {:ok, game} =
        Games.update_game(game, %{
          current_game_question_id: game_question.id,
          state: "asking_question",
          state_ends_at: DateTime.add(now, 30) |> DateTime.truncate(:second)
        })

      # Start the game service up and check the state
      start_supervised!({GameService, game.id})
      current = GameService.current(game.id)

      assert current.question_count == 3
      assert current.question_number == 2
      assert current.state == "asking_question"
      assert current.game_question_id == game_question.id
    end
  end
end
