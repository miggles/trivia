defmodule Trivia.Factory do
  use ExMachina.Ecto, repo: Trivia.Repo

  def user_factory do
    %Trivia.Accounts.User{
      username: sequence(:username, &"username#{&1}")
    }
  end

  def game_factory do
    {:ok, now} = DateTime.now("Etc/UTC")

    then =
      now
      |> DateTime.add(30)
      |> DateTime.truncate(:second)

    %Trivia.Games.Game{
      user: build(:user),
      slug: sequence(:slug, &"slug#{&1}"),
      state: "waiting_to_start",
      state_ends_at: then,
      number_of_questions: 1,
      starting_cash: 100,
      wait_duration: 30,
      ask_duration: 30,
      bid_duration: 30,
      display_duration: 30
    }
  end

  def question_factory do
    %Trivia.Questions.Question{
      user: params_for(:user),
      question: "Question?",
      unit: "m",
      answer: 100,
      citation: "~~~baleeted~~~"
    }
  end

  def game_question_factory do
    %Trivia.Games.GameQuestion{
      question: build(:question),
      game: build(:game)
    }
  end

  def game_user_factory do
    %Trivia.Games.GameUser{
      user: build(:user),
      game: build(:game),
      cash: 100
    }
  end

  def game_question_user_factory do
    %Trivia.Games.GameQuestionUser{
      game_user: build(:game_user),
      game_question: build(:game_question),
      answer: 50,
      delay: sequence(:delay, &(&1 * 1_000))
    }
  end

  def game_question_answer_factory do
    %Trivia.Games.GameQuestionAnswer{
      game_question: build(:game_question),
      answer: 99,
      winner: true,
      correct: false,
      payout_rate: 2.0,
      payout_string: "2:1"
    }
  end

  def game_question_answer_game_user_factory do
    %Trivia.Games.GameQuestionAnswerGameUser{
      game_question_answer: build(:game_question_answer),
      game_user: build(:game_user)
    }
  end

  def game_question_answer_game_user_bid_factory do
    %Trivia.Games.GameQuestionAnswerGameUserBid{
      game_question_answer: build(:game_question_answer),
      game_user: build(:game_user),
      bid: 200,
      payout: 400
    }
  end

  def game_user_loan_factory do
    %Trivia.Games.GameUserLoan{
      game_user: build(:game_user),
      amount: 200,
      amount_owing: 400,
      interest_rate: 2.0,
      interest_rate_string: "2.0x"
    }
  end

  # def article_factory do
  #   title = sequence(:title, &"Use ExMachina! (Part #{&1})")
  #   # derived attribute
  #   slug = MyApp.Article.title_to_slug(title)
  #   %MyApp.Article{
  #     title: title,
  #     slug: slug,
  #     # associations are inserted when you call `insert`
  #     author: build(:user),
  #   }
  # end

  # # derived factory
  # def featured_article_factory do
  #   struct!(
  #     article_factory(),
  #     %{
  #       featured: true,
  #     }
  #   )
  # end

  # def comment_factory do
  #   %MyApp.Comment{
  #     text: "It's great!",
  #     article: build(:article),
  #   }
  # end
end
